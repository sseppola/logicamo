package com.abo.logicamo.client.graphics;

public class Point {

	public static final Point ZERO = new Point(0, 0);

	public double x;
	public double y;

	public Point() {
		x = 0;
		y = 0;
	}

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Point(Point p) {
		x = p.x;
		y = p.y;
	}

	public double distance(Point p) {
		double dx = p.x - x;
		double dy = p.y - y;

		return Math.sqrt((dx * dx) + (dy * dy));
	}

	public double distance(double px, double py) {
		double dx = px - x;
		double dy = py - y;

		return Math.sqrt((dx * dx) + (dy * dy));
	}

	public double distance2(Point p) {
		double dx = p.x - x;
		double dy = p.y - y;

		return (dx * dx) + (dy * dy);
	}

	public double distance2(double px, double py) {
		double dx = px - x;
		double dy = py - y;

		return (dx * dx) + (dy * dy);
	}
}
