package com.abo.logicamo.client.graphics.loader;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.abo.logicamo.client.Logger;
import com.google.gwt.event.dom.client.ErrorEvent;
import com.google.gwt.event.dom.client.ErrorHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Improved Image Loader
 * 
 * This was written in the final stages of Logicamo development to allow for
 * cleaner handling of resource management.
 * 
 * This Loader contains a simple asset management system and is presented as a
 * singleton, so as to allow for easier (yet still debuggable) resource access
 * across the application.
 * 
 * The program was originally written using a very simple loading system, which
 * (due to its simplicity) made a lot of code brittle and difficult to maintain.
 * 
 * (c) 2015 Patrik Lindström, Åbo Akademi
 * 
 * Licensed under WTFPL 2.0
 * 
 * See http://www.wtfpl.net/txt/copying for full license text.
 */
public class Loader {

	// This thing was written in a hurry and turned into a mess itself, but at
	// the very least the API looks clean to the outside...

	/**
	 * Callback for single item loading method.
	 */
	private static interface ItemCallback {
		public void onComplete(String id, Image image);
	}

	/**
	 * Callback interface for batch loading method.
	 */
	public static interface BatchCallback {
		public void onComplete();
	}

	private static final Loader instance = new Loader();

	// Asset storage maps
	private final Map<String, Image> idToImageMap;
	private final Map<String, Image> urlToImageMap;
	private final Map<Image, String> imageToIdMap;

	// We don't want to rely on Guava containers, so no bi- or multimap..
	private final Map<Image, HandlerRegistration> loadCompleteHandlers;
	private final Map<Image, HandlerRegistration> loadErrorHandlers;

	// List of item completion callbacks attached to image
	private final Map<Image, ItemCallback> itemCallbacks;

	// Set of images still to load
	private final Set<Image> loadQueue;

	private Loader() {
		idToImageMap = new HashMap<>();
		urlToImageMap = new HashMap<>();
		imageToIdMap = new HashMap<>();
		loadCompleteHandlers = new HashMap<>();
		loadErrorHandlers = new HashMap<>();
		itemCallbacks = new HashMap<>();
		loadQueue = new HashSet<>();
	}

	// Clean up image dependencies
	private void finalizeImage(Image img) {
		loadQueue.remove(img);
		try {
			img.removeFromParent();
		} catch (Exception ignore) {
		}
		loadCompleteHandlers.remove(img).removeHandler();
		loadErrorHandlers.remove(img).removeHandler();
	}

	// Call associated handler when image has been successfully loaded
	private LoadHandler imageLoadHandler = new LoadHandler() {
		@Override
		public void onLoad(LoadEvent event) {
			Image img = (Image) event.getSource();
			finalizeImage(img);
			ItemCallback handler = itemCallbacks.remove(img);
			String id = imageToIdMap.get(img);
			handler.onComplete(id, img);
			Logger.log(instance, "Image " + img.getUrl() + " loaded");
		}
	};

	// Cause assertion failure when an image does not load correctly
	private ErrorHandler imageErrorHandler = new ErrorHandler() {
		@Override
		public void onError(ErrorEvent event) {
			Image img = (Image) event.getSource();
			finalizeImage(img);
			Logger.error(instance, "Image " + img + " with url " + img.getUrl() + " failed to load");
			assert false : "Image failed to load";
		}
	};

	/**
	 * Load a single image
	 * 
	 * @param id
	 *            ID of the image
	 * @param url
	 *            URL of the image
	 * @param callback
	 *            callback to call when the image has been loaded
	 */
	private void loadImage(String id, String url, ItemCallback callback) {
		Image img = null;
		if (idToImageMap.containsKey(id)) {
			img = idToImageMap.get(id);
		} else if (urlToImageMap.containsKey(url)) {
			img = urlToImageMap.get(url);
			idToImageMap.put(id, img);
			imageToIdMap.put(img, id);
		}

		if (img != null) {
			// Report image complete (even if it wouldn't be that in reality)
			callback.onComplete(id, img);
			return;
		}

		//
		// Actually load an image
		//

		// Create image object
		img = new Image();

		// Register callbacks first
		loadCompleteHandlers.put(img, img.addLoadHandler(imageLoadHandler));
		loadErrorHandlers.put(img, img.addErrorHandler(imageErrorHandler));
		itemCallbacks.put(img, callback);
		loadQueue.add(img);

		// Register the image in the asset list
		urlToImageMap.put(url, img);
		idToImageMap.put(id, img);
		imageToIdMap.put(img, id);

		// Set the url, make the image invisible and dip it in the
		// root panel for a while to get it to load
		img.setUrl(url);
		img.setVisible(false);
		RootPanel.get().add(img);
	}

	/**
	 * Load a several images
	 * 
	 * @param idToUrlMap
	 *            String if ID to URLs where image files can be found
	 * @param callback
	 *            callback function to run when loading completes
	 */
	public static void loadBatch(Map<String, String> idToUrlMap, final BatchCallback callback) {

		final ItemCallback icb = new ItemCallback() {
			@Override
			public void onComplete(String id, Image image) {
				if (instance.loadQueue.isEmpty()) {
					callback.onComplete();
				}
			}
		};

		for (String id : idToUrlMap.keySet()) {
			String url = idToUrlMap.get(id);
			instance.loadImage(id, url, icb);
		}

	}

	/**
	 * Get access to Image object by its ID. Request for a nonexistent ID causes
	 * an assertion failure.
	 * 
	 * @param id
	 *            id string of desired image
	 * @return an Image object
	 */
	public static Image getImageById(String id) {
		Image img = instance.idToImageMap.get(id);
		if (img == null) {
			Logger.error(instance, "Image with ID \"" + id + "\" has not been loaded");
			assert false;
		}
		return img;
	}

	/**
	 * Get access to Image object by its URL. Request for a nonexistent URL
	 * causes an assertion failure.
	 * 
	 * @param url
	 *            url of the desired image
	 * @return an Image object
	 */
	public static Image getImageByUrl(String url) {
		Image img = instance.urlToImageMap.get(url);
		if (img == null) {
			Logger.error(instance, "Image with URL \"" + url + "\" has not been loaded");
			assert false;
		}
		return img;
	}

}
