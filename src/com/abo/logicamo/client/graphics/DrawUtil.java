package com.abo.logicamo.client.graphics;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;

public abstract class DrawUtil {

	public static void drawRoundRect(Context2d ctx, double x, double y, double width, double height, double radius,
			String fillColor, String strokeColor) {

		ctx.beginPath();
		ctx.moveTo(x + radius, y);
		ctx.lineTo(x + width - radius, y);
		ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
		ctx.lineTo(x + width, y + height - radius);
		ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
		ctx.lineTo(x + radius, y + height);
		ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
		ctx.lineTo(x, y + radius);
		ctx.quadraticCurveTo(x, y, x + radius, y);
		ctx.closePath();

		if (fillColor != null) {
			ctx.setFillStyle(fillColor);
			ctx.fill();
		}
		if (strokeColor != null) {
			ctx.setStrokeStyle(strokeColor);
			ctx.stroke();
		}

		ctx.beginPath();
		
	}

	public static void setCanvasSize(Canvas content, int width, int height) {
		content.setWidth(width + "px");
		content.setHeight(height + "px");
		content.setCoordinateSpaceWidth(width);
		content.setCoordinateSpaceHeight(height);
	}

}
