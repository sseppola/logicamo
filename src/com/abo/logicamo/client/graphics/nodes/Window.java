package com.abo.logicamo.client.graphics.nodes;

import com.abo.logicamo.client.graphics.DrawUtil;
import com.abo.logicamo.client.graphics.Node;
import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.nodes.Button.ButtonHandler;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.Context2d.TextBaseline;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;

public class Window extends Node {

	private Canvas frame;
	private Canvas content;
	private Text title;
	private Button closeButton;
	private int frameWidth;
	private int titleHeight;

	public Window() {
		this("Untitled window");
	}

	public Window(String title) {
		this(title, null);
	}

	public Window(String title, Canvas content) {

		// Create title text
		this.title = new Text(title);
		this.title.setFont(12, "Arial");
		add(this.title);

		// Assign content canvas
		this.content = content;

		// Create a close button
		closeButton = new Button(Loader.getImageById("button_remove"), new ButtonHandler() {
			@Override
			public void onRelease(Button b) {

			}

			@Override
			public void onPress(Button b) {

			}

			@Override
			public void onClick(Button b) {
				Window.this.removeFromParent();
			}
		});

		// Position the close button in the top-left corner
		double half = closeButton.getWidth() / 2;
		closeButton.setPosition(-half, -half);
		add(closeButton);

		// Create the frame canvas
		frame = Canvas.createIfSupported();

		// Set a default size (causes redraw of the frame)
		titleHeight = 25;
		frameWidth = 2;
		setSize(300, 300);

		// Enable picking to make it possible to drag the window around
		setPickingEnabled(true);
	}

	public String getTitle() {
		return title.getText();
	}

	public void setTitle(String title) {
		final int titlePadding = 25;
		if (this.title.getWidth() > this.getWidth() - frameWidth * 2 + titlePadding) {
			setSize(this.title.getWidth() - frameWidth * 2 + titlePadding,
					this.getHeight() - titleHeight - frameWidth * 2);
		}
		drawFrame();
	}

	public Canvas getContent() {
		return content;
	}

	public void setContent(Canvas content) {
		this.content = content;
	}

	private void positionTitle() {
		title.setTextBaseline(TextBaseline.MIDDLE);
		title.setPosition(frameWidth + 5, frameWidth + titleHeight / 2);
	}

	public void setFrameSize(int frameWidth, int titlebarHeight) {
		this.frameWidth = frameWidth;
		this.titleHeight = titlebarHeight;
		positionTitle();
		drawFrame();
	}

	@Override
	public void setSize(double w, double h) {
		w += frameWidth * 2;
		h += titleHeight + frameWidth * 3;

		if (w < title.getWidth() + 25) {
			w = title.getWidth() + 25;
		}

		super.setSize(w, h);
		DrawUtil.setCanvasSize(frame, (int)w, (int)h);

		positionTitle();
		drawFrame();
	}

	public int getContentAreaWidth() {
		return (int) (getWidth() - frameWidth * 2);
	}

	public int getContentAreaHeight() {
		return (int) (getHeight() - frameWidth * 3 - titleHeight);
	}

	private void drawFrame() {
		Context2d ctx = frame.getContext2d();
		ctx.clearRect(0, 0, frame.getCoordinateSpaceWidth(), frame.getCoordinateSpaceHeight());

		final int width = frame.getCoordinateSpaceWidth();
		final int height = frame.getCoordinateSpaceHeight();
		final int r = 5;
		
		// Draw outer frame
		DrawUtil.drawRoundRect(ctx, 0, 0, 
				width, height, r, 
				"lightgrey", "darkgrey");
		
		// Draw titlebar area
		DrawUtil.drawRoundRect(ctx, frameWidth, frameWidth, 
				width - frameWidth * 2, titleHeight, r, 
				"white", "darkgrey");
	}

	@Override
	public void draw(Context2d ctx) {

		ctx.setShadowBlur(15);
		ctx.setShadowColor("black");
		ctx.setShadowOffsetX(0);
		ctx.setShadowOffsetY(0);

		ctx.drawImage(frame.getCanvasElement(), 0, 0);
		ctx.setShadowBlur(0);
		ctx.setShadowColor("rgba(0,0,0,0)");

		if (content != null) {
			ctx.drawImage(content.getCanvasElement(), frameWidth, titleHeight + frameWidth * 2);
		}

		update();
	}

	public void update() {

		if (dragging && !isMouseDown()) {
			stopDrag();
		}

		if (isMousePressed() && isMouseOver() && !dragging) {
			startDrag();

			// When GWT main loop updates, move this window to the
			// top of the draw stack
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {
				@Override
				public void execute() {
					bringToFront();
				}
			});
		}

		if (isMouseOver() || closeButton.isMouseOver()) {
			closeButton.setOpacity(1.0);
		} else {
			closeButton.setOpacity(0.0);
		}

	}

}
