package com.abo.logicamo.client.graphics.nodes;

import com.abo.logicamo.client.graphics.Node;
import com.google.gwt.canvas.dom.client.Context2d;

public class Rectangle extends Node {

	private String colour = "grey";
	private boolean rounded;

	public Rectangle(boolean rounded) {
		this.rounded = rounded;
	}

	public void draw(Context2d ctx) {
		if (rounded) {
			drawRounded(ctx);
		}
		else {
			ctx.setFillStyle(colour);
			ctx.setLineWidth(1);
			ctx.fillRect(0, 0, getWidth(), getHeight());
		}
	}

	public void drawRounded(Context2d ctx) {
		double r = 5.0;
		double w = getWidth();
		double h = getHeight();
		
		if (w < (2 * r)) {
			r = w / 2;
		}
		if (h < (2 * r)) {
			r = h / 2;
		}

		ctx.setFillStyle(colour);
		ctx.setLineWidth(1);
		ctx.beginPath();
		ctx.moveTo(r, 0);
		ctx.arcTo(w, 0, w, h, r);
		ctx.arcTo(w, h, 0, h, r);
		ctx.arcTo(0, h, 0, 0, r);
		ctx.arcTo(0, 0, w, 0, r);
		ctx.closePath();
		ctx.fill();
		//return this;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}
}
