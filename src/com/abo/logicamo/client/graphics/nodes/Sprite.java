package com.abo.logicamo.client.graphics.nodes;

import com.abo.logicamo.client.graphics.Node;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.user.client.ui.Image;

public class Sprite extends Node {

	private boolean usesCanvas;
	private Element elem;

	public Sprite() {
		setPickingEnabled(true);
		elem = null;
		usesCanvas = false;
	}

	public Sprite(Image img) {
		this();
		assert (img != null) : "Image should not be null";
		setImage(img);
	}

	@Override
	public void draw(Context2d ctx) {
		if (elem != null) {
			if (usesCanvas) {
				ctx.drawImage((CanvasElement) elem, 0, 0);
			} else {
				ctx.drawImage((ImageElement) elem, 0, 0);
			}
		}
	}

	public void setImage(Image i) {
		elem = i.getElement();
		setSize(i.getWidth(), i.getHeight());
		usesCanvas = false;
	}

	public void setCanvas(Canvas c) {
		elem = c.getElement();
		setSize(c.getCoordinateSpaceWidth(), c.getCoordinateSpaceHeight());
		usesCanvas = true;
	}
}