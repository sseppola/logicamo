package com.abo.logicamo.client.graphics.nodes;

import com.google.gwt.user.client.ui.Image;

public class Button extends Sprite {

	/**
	 * Callback interface for button actions
	 */
	public interface ButtonHandler {

		/**
		 * Called when a button has been held down and then released
		 * 
		 * @param b
		 *            reference to the button calling the handler
		 */
		void onClick(Button b);

		/**
		 * Called while a button is pressed down
		 * 
		 * @param b
		 *            reference to the button calling the handler
		 */
		void onPress(Button b);

		/**
		 * Called when a previously pressed button is released
		 * 
		 * @param b
		 *            reference to the button calling the handler
		 */
		void onRelease(Button b);

	}

	private ButtonHandler _handler = null;
	private int _state = 0;

	public Button(Image img, ButtonHandler handler) {
		super(img);
		_handler = handler;
	}

	public void update() {

		switch(_state) {
		case 0:
			if (isMouseOver() && !isMouseDown()) {
				_state = 1;
			}
		break;
		case 1:
			if (isMouseOver()) {
				if(isMouseDown()) {
					_handler.onPress(this);
					_state = 2;
				}
			} else {
				_state = 0;
			}
		break;
		case 2:
			if (isMouseOver() && isMouseReleased()) {
				_handler.onClick(this);
			}

			if (isMouseReleased()) {
				_handler.onRelease(this);
				_state = 0;
			}

		break;
		}
	}
}
