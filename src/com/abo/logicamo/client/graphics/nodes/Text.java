package com.abo.logicamo.client.graphics.nodes;

import com.abo.logicamo.client.graphics.Engine;
import com.abo.logicamo.client.graphics.Node;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.Context2d.TextAlign;
import com.google.gwt.canvas.dom.client.Context2d.TextBaseline;
import com.google.gwt.canvas.dom.client.TextMetrics;

public class Text extends Node {

	private String text_val;
	private String font_style;
	private String colour;
	private TextBaseline tb;
	private int size;

	public Text(String text) {
		size = 20;
		setFont(size, "Arial");
		setText(text);
		colour = "black";
		tb = TextBaseline.BOTTOM;
		recalculateSize();
	}

	public void setText(String text) {
		text_val = text;
		recalculateSize();
	}

	public String getText() {
		return text_val;
	}
	
	public void setColour(String colour) {
		this.colour = colour;
	}

	public int getFontSize() {
		return size;
	}

	public void setFont(int size, String font) {
		this.size = size;
		font_style = size + "px " + font;
		setSize(getWidth(), size);
		recalculateSize();
	}

	public void setTextBaseline(TextBaseline tb) {
		this.tb = tb;
		recalculateSize();
	}

	private void recalculateSize() {
		Context2d ctx = Engine.getTempContext();
		ctx.setLineWidth(1);
		ctx.setFont(font_style);
		TextMetrics m = ctx.measureText(text_val);
		setSize(m.getWidth(), getHeight());
	}

	public void draw(Context2d ctx) {
		ctx.setFillStyle(colour);
		ctx.setLineWidth(1);
		ctx.setFont(font_style);
		ctx.setTextAlign(TextAlign.LEFT);
		ctx.setTextBaseline(tb);
		ctx.fillText(text_val, 0, 0);
	}
}
