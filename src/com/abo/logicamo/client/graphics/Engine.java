package com.abo.logicamo.client.graphics;

import java.util.ArrayList;
import java.util.List;

import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.MainLoop;
import com.google.gwt.animation.client.AnimationScheduler;
import com.google.gwt.animation.client.AnimationScheduler.AnimationCallback;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;

/**
 * Application life-time and input/output management code.
 */
public class Engine {

	public interface ResizeHandler {
		public void onResize(int newWidth, int newHeight);
	}

	private static Canvas _tempCanvas = null;

	public static Context2d getTempContext() {
		if (_tempCanvas == null) {
			_tempCanvas = Canvas.createIfSupported();
		}
		return _tempCanvas.getContext2d();
	}

	private List<ResizeHandler> resizeHandlers;
	private AnimationScheduler scheduler;
	private AnimationCallback callback;
	private boolean running;

	private double time_now;
	private double time_last;
	private double time_delta;

	private double mouseX;
	private double mouseY;
	private boolean mouseDown;

	private Scene scene;
	private MainLoop mainloop;
	private Canvas canvas;

	private int canvasWidth;
	private int canvasHeight;
	
	public Engine(Canvas target) {

		resizeHandlers = new ArrayList<>();

		time_last = 0;
		time_now = 0;
		time_delta = 0;
		running = false;
		canvas = target;
		canvasWidth = canvas.getOffsetWidth();
		canvasHeight = canvas.getOffsetHeight();
		scene = new Scene(target);
		scheduler = AnimationScheduler.get();

		callback = new AnimationCallback() {
			@Override
			public void execute(double timestamp) {
				update(timestamp);
				if (running) {
					scheduler.requestAnimationFrame(callback, canvas.getElement());
				}
			}
		};

		canvas.addMouseDownHandler(new MouseDownHandler() {
			@Override
			public void onMouseDown(MouseDownEvent event) {
				mouseX = event.getX();
				mouseY = event.getY();
				mouseDown = true;
			}
		});

		canvas.addMouseMoveHandler(new MouseMoveHandler() {
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				mouseX = event.getX();
				mouseY = event.getY();
			}
		});

		canvas.addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent event) {
				mouseX = event.getX();
				mouseY = event.getY();
				mouseDown = false;
			}
		});
		
		// Create a timer responsible for performing window resizing in a deferred manner
		// Calls resize handlers, so that the rest of the program can respond to screen
		// size changes.
		final Timer resizeTimer = new Timer() {
			@Override
			public void run() {
				int w = Window.getClientWidth();
				int h = Window.getClientHeight();
				canvas.setSize(w + "px", h + "px");
				canvas.setCoordinateSpaceWidth(w);
				canvas.setCoordinateSpaceHeight(h);
				canvas.setPixelSize(w, h);
				canvasWidth = w;
				canvasHeight = h;
				for (ResizeHandler handler : resizeHandlers) {
					handler.onResize(w, h);
				}
			}
		};

		// Handle window resizing in a timely manner
		Window.addResizeHandler(new com.google.gwt.event.logical.shared.ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				resizeTimer.cancel();
				resizeTimer.schedule(16);
			}
		});

		// Schedule one go of the timer immediately to get
		// canvas size as soon as control is yielded to the browser
		resizeTimer.schedule(0);
	}

	public void addResizeHandler(ResizeHandler h) {
		if (!resizeHandlers.contains(h)) {
			resizeHandlers.add(h);
		}
	}

	public void removeResizeHandler(ResizeHandler h) {
		resizeHandlers.remove(h);
	}

	public void setMainLoop(MainLoop mainloop) {
		Logger.log(this, "mainloop is set");
		this.mainloop = mainloop;
	}

	public double getTimeDelta() {
		return time_delta;
	}

	public Context2d getContext() {
		return canvas.getContext2d();
	}

	public Canvas getCanvas() {
		return canvas;
	}

	public int getScreenWidth() {
		return canvasWidth;
	}

	public int getScreenHeight() {
		return canvasHeight;
	}

	public void update(double time) {

		time_last = time_now;
		time_now = time;
		time_delta = time_now - time_last;

		// Update mouse logic
		scene.updateMouse(mouseX, mouseY, mouseDown);

		// Run application logic
		if (mainloop != null) {
			mainloop.update();
		}

		// Draw stuff to screen
		scene.draw();
	}

	public void start() {
		Logger.log(this, "engine is started");
		running = true;
		scheduler.requestAnimationFrame(callback, canvas.getElement());
	}

	public boolean isMouseDown() {
		return mouseDown;
	}

	public boolean isMouseUp() {
		return !mouseDown;
	}

	public double getMouseX() {
		return mouseX;
	}

	public double getMouseY() {
		return mouseY;
	}

	public void stop() {
		running = false;
	}

	public void add(Node n) {
		scene.add(n);
	}

	public void removeNode(Node n) {
		scene.removeNode(n);
	}

	public void clear() {
		scene.clearChildren();
	}

	public Node getNode(int i) {
		return scene.getNode(i);
	}

	public Node getRootNode() {
		return scene.getRootNode();
	}
	
}