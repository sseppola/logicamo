package com.abo.logicamo.client.graphics;

import java.util.ArrayList;

import com.google.gwt.canvas.dom.client.Context2d;

public class Node {

	public ArrayList<Node> children = new ArrayList<Node>();

	private double x;
	private double y;
	private double width;
	private double height;
	private double opacity;

	private boolean visible;
	private boolean picking;

	protected boolean mouse_over_now; // Set to true by engine if mouse is over
										// this node
	protected boolean mouse_down_now; // Set to true by engine if mouse is
										// pressed while over this node
	protected boolean mouse_over_last; // Set to true by engine if mouse is over
										// this node
	protected boolean mouse_down_last; // Set to true by engine if mouse is
										// pressed while over this node

	protected double mouse_x; // Set to X coordinate of mouse relative to this
								// node
	protected double mouse_y; // Set to Y coordinate of mouse relative to this
								// node

	protected boolean dragging;
	protected boolean drag_start;
	protected double drag_x; // mouse coordinates when dragging starts
	protected double drag_y;

	protected Node parent;

	public Node() {
		opacity = 1.0;
		parent = null;
		picking = false;
		dragging = false;
		drag_start = false;

		mouse_over_now = false;
		mouse_over_last = false;
		mouse_down_now = false;
		mouse_down_last = false;

		mouse_x = 0;
		mouse_y = 0;
		drag_x = 0;
		drag_y = 0;

		visible = true;
		x = 0;
		y = 0;
		width = 0;
		height = 0;
	}

	public void startDrag() {
		drag_start = true;
	}

	public void stopDrag() {
		drag_start = false;
		dragging = false;
	}
	
	protected void initDrag() {
		drag_start = false;
		drag_x = mouse_x;
		drag_y = mouse_y;
		dragging = true;
	}

	protected void updateDrag() {
		if (dragging) {
			double dx = mouse_x - drag_x;
			double dy = mouse_y - drag_y;
			setPosition(x + dx, y + dy);
		}
	}

	public void setVisible(boolean b) {
		visible = b;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setOpacity(double o) {
		opacity = Math.max(0, Math.min(o, 1));
		if (opacity == 0) {
			setVisible(false);
		} else {
			setVisible(true);
		}
	}

	public double getOpacity() {
		return opacity;
	}

	public void add(Node n) {
		n.removeFromParent();
		children.add(n);
		n.parent = this;
	}

	public void remove(Node n) throws Error {
		if (n.parent == this) {
			children.remove(children.indexOf(n));
			n.parent = null;
		} else {
			throw new Error("Node " + n + " is not a child of " + this);
		}
	}

	public void removeFromParent() {
		if (parent != null) {
			parent.remove(this);
		}
	}

	public Node getParent() {
		return parent;
	}

	public void clear() {
		children.clear();
	}

	public void draw(Context2d ctx) {

	}

	public boolean containsPoint(double x, double y) {
		return (x >= 0) && (x < width) && (y >= 0) && (y < height);
	}

	public boolean didMouseEnter() {
		return mouse_over_now && !mouse_over_last;
	}

	public boolean isMouseOver() {
		return mouse_over_now;
	}

	public boolean didMouseLeave() {
		return !mouse_over_now && mouse_over_last;
	}

	public void bringToFront() {
		parent.children.remove(this);
		parent.children.add(this);
	}

	public boolean isMousePressed() {

		return mouse_down_now && !mouse_down_last;
	}

	public boolean isMouseDown() {

		return mouse_down_now;
	}

	public boolean isMouseReleased() {

		return !mouse_down_now && mouse_down_last;
	}

	public void setPickingEnabled(boolean b) {
		picking = b;
	}

	public boolean isPickingEnabled() {
		return picking;
	}

	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public void setSize(double w, double h) {
		this.width = w;
		this.height = h;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public double getMouseX() {
		return mouse_x;
	}

	public double getMouseY() {
		return mouse_y;
	}

	public Point localToGlobal(Point p) {

		double dx = x + p.x;
		double dy = y + p.y;

		Node n = parent;

		while (n != null) {
			dx = dx + n.x;
			dy = dy + n.y;
			n = n.parent;
		}

		return new Point(dx, dy);
	}

	public Point globalToLocal(Point p) {

		double dx = p.x - x;
		double dy = p.y - y;

		Node n = parent;

		while (n != null) {
			dx = dx - n.x;
			dy = dy - n.y;
			n = n.parent;
		}

		return new Point(dx, dy);
	}

	public boolean testIntersection(Node n) {

		Point p = localToGlobal(Point.ZERO);
		Point np = n.localToGlobal(Point.ZERO);

		boolean horizontalSharedPoints = ((p.x + width) >= np.x)
				&& ((np.x + n.width) >= p.x);
		boolean verticalSharedPoints = ((p.y + height) >= np.y)
				&& ((np.y + height) >= p.y);

		return horizontalSharedPoints && verticalSharedPoints;

	}
}
