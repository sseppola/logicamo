package com.abo.logicamo.client.graphics;

import java.util.Stack;

import com.abo.logicamo.client.Globals;
import com.abo.logicamo.client.graphics.nodes.Button;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;

/**
 * Rendering loop and Node state management code
 */
public class Scene {

	private final Canvas canvas;
	private final Context2d ctx;
	private final Node rootNode;
	private final String clearColor = Globals.clear_col;
	private final Stack<Button> processedButtons;
	private Node picked;
	private boolean autoclear;

	public Scene(Canvas c) {
		processedButtons = new Stack<>();
		canvas = c;
		ctx = canvas.getContext2d();
		rootNode = new Node();
		picked = null;
		autoclear = true;
	}

	public Node getPickedNode() {
		return picked;
	}

	public void add(Node n) {
		rootNode.add(n);
	}

	public void removeNode(Node n) {
		rootNode.remove(n);
	}

	public void clearChildren() {
		rootNode.clear();
	}

	public Node getNode(int i) {
		return rootNode.children.get(i);
	}

	public Node getRootNode() {
		return rootNode;
	}
	
	public void clear() {
		ctx.setTransform(1, 0, 0, 1, 0, 0);
		ctx.setFillStyle(clearColor);
		ctx.setGlobalAlpha(1.0);
		ctx.fillRect(0, 0, canvas.getCoordinateSpaceWidth(), canvas.getCoordinateSpaceHeight());
	}
	
	public void setAutoClear(boolean enable) {
		autoclear = enable;
	}
	
	public void draw() {
		if(autoclear) {
			clear();
		}

		drawRecursive(rootNode, rootNode.getX(), rootNode.getY(), rootNode.getOpacity());
	}

	private void drawRecursive(Node parent, double parentX, double parentY, double parentAlpha) {

		int childrenSize = parent.children.size();
		for (int i = 0; i < childrenSize; i++) {
			Node child = parent.children.get(i);

			if (child.isVisible()) {

				ctx.setTransform(1, 0, 0, 1, parentX + child.getX(), parentY + child.getY());
				ctx.setGlobalAlpha(child.getOpacity() * parentAlpha);
				child.draw(ctx);

				drawRecursive(child, parentX + child.getX(), parentY + child.getY(), child.getOpacity() * parentAlpha);

			}

		}

	}

	public void updateMouse(double mouseX, double mouseY, boolean pressed) {

		picked = null;

		updateMouseRecursive(rootNode, rootNode.getX(), rootNode.getY(), mouseX, mouseY, pressed);

		if (picked != null) {
			picked.mouse_over_now = true;
		}
		
		for(Button b : processedButtons) {
			b.update();
		}
		processedButtons.clear();

	}

	private void updateMouseRecursive(Node parent, double parentX, double parentY, double mouseX, double mouseY, boolean pressed) {

		for (Node child : parent.children) {

			child.mouse_down_last = child.mouse_down_now;
			child.mouse_over_last = child.mouse_over_now;
			child.mouse_down_now = pressed;
			child.mouse_over_now = false;
			child.mouse_x = mouseX - parentX - child.getX();
			child.mouse_y = mouseY - parentY - child.getY();

			// Picked becomes the topmost hovered Node
			if (child.isPickingEnabled()) {
				if ((child.mouse_x >= 0) && (child.mouse_x < child.getWidth()) && (child.mouse_y >= 0) && (child.mouse_y < child.getHeight())) {
					picked = child;
				}
			}

			if(child.drag_start) {
				child.initDrag();
			}
			
			if (child.dragging) {
				child.updateDrag();
			}
			
			if(child instanceof Button) {
				processedButtons.add(((Button)child));
			}

			if (!child.children.isEmpty()) {
				updateMouseRecursive(child, parentX + child.getX(), parentY + child.getY(), mouseX, mouseY, pressed);
			}
			
		}

	}

}
