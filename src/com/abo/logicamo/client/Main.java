package com.abo.logicamo.client;

import java.util.HashMap;
import java.util.Map;

import com.abo.logicamo.client.graphics.DrawUtil;
import com.abo.logicamo.client.graphics.Engine;
import com.abo.logicamo.client.graphics.Scene;
import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.loader.Loader.BatchCallback;
import com.abo.logicamo.client.graphics.nodes.Text;
import com.abo.logicamo.client.graphics.nodes.Window;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d.TextBaseline;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Main implements EntryPoint {

	/**
	 * Updates the canvas on a high frequency so that elements move smoothly.
	 */
	private Engine engine;

	/**
	 * Runs the program logic.
	 */
	private MainLoop mainloop;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		// Initialize core objects
		createMenuBar();
		createEngine();

		// Start image loading process
		// This calls setup() when it completes
		loadImages();
	}
	
	/**
	 * Create the canvas and graphics engine
	 */
	private void createEngine() {
		final Canvas canvas = Canvas.createIfSupported();
		canvas.setHeight("100%");
		canvas.setWidth("100%");
		RootPanel.get().add(canvas);

		engine = new Engine(canvas);
		Globals.mainEngine = engine;
	}
	
	/**
	 * This is called by the imageloader after all the images are loaded
	 */
	public void setup() {
		Logger.log(this, "Logicamo v.1.3 :: setup()");
		mainloop = new MainLoop(engine);
		engine.setMainLoop(mainloop);
		engine.start();
	}

	/**
	 * DANGER! This method is the only place where loadable assets are defined -
	 * add ALL images you wish to preload HERE.
	 */
	private void loadImages() {
		Map<String, String> assetMap = new HashMap<>();

		//
		// assetMap.put(ID OF IMAGE, URL OF IMAGE);
		//

		// Default connector
		assetMap.put("connector_active", "connectors/k_connector_pink.png");
		assetMap.put("connector_inactive", "connectors/k_connector_grey.png");

		// Right side flat connector
		assetMap.put("connector_back_active", "connectors/k_connector_pink_back.png");
		assetMap.put("connector_back_inactive", "connectors/k_connector_grey_back.png");

		// Left side flat connector
		assetMap.put("connector_front_active", "connectors/k_connector_pink_front.png");
		assetMap.put("connector_front_inactive", "connectors/k_connector_grey_front.png");

		// Workspace gates
		assetMap.put("gate_and", "gates/k_and.png");
		assetMap.put("gate_nand", "gates/k_nand.png");
		assetMap.put("gate_or", "gates/k_or.png");
		assetMap.put("gate_nor", "gates/k_nor.png");
		assetMap.put("gate_xor", "gates/k_xor.png");
		assetMap.put("gate_xnor", "gates/k_xnor.png");
		assetMap.put("gate_not", "gates/k_not.png");

		// Gatebar gate creation buttons
		assetMap.put("gate_and_button", "gates/k_and_creator.png");
		assetMap.put("gate_nand_button", "gates/k_nand_creator.png");
		assetMap.put("gate_or_button", "gates/k_or_creator.png");
		assetMap.put("gate_nor_button", "gates/k_nor_creator.png");
		assetMap.put("gate_xor_button", "gates/k_xor_creator.png");
		assetMap.put("gate_xnor_button", "gates/k_xnor_creator.png");
		assetMap.put("gate_not_button", "gates/k_not_creator.png");

		//
		// Input and output gate images
		//

		// With neutral (no) signal
		assetMap.put("input_gate", "gates/input.png");
		assetMap.put("output_gate", "gates/input.png");

		// With low signal
		assetMap.put("input_gate_zero", "gates/input_zero.png");
		assetMap.put("output_gate_zero", "gates/input_zero.png");

		// With high signal
		assetMap.put("input_gate_one", "gates/input_one.png");
		assetMap.put("output_gate_one", "gates/input_one.png");

		// New gate creation button
		assetMap.put("input_gate_button", "gates/input_creator.png");
		assetMap.put("output_gate_button", "gates/input_creator.png");

		// Remove button
		assetMap.put("button_remove", "gates/remove_icon.png");
		assetMap.put("button_clear", "gates/clear_icon.png");

		//
		// Call Loader to do a batch load using our asset map,
		// and then start the actual program when ready
		//

		Loader.loadBatch(assetMap, new BatchCallback() {
			@Override
			public void onComplete() {
				setup();
			}
		});
	}
	
	private Window createAboutBox(String aboutText) {
		Canvas content = Canvas.createIfSupported();
		
		int width = 400;
		
		int i = 0;
		Scene scn = new Scene(content);
		scn.setAutoClear(false);
		for(String s : aboutText.split("\n")) {
			Text t = new Text(s);
			t.setFont(14, "Arial");
			t.setTextBaseline(TextBaseline.TOP);
			t.setPosition(5, 5 + ((14 + 2) * i++));
			if(width < t.getWidth() + 10) {
				width = (int) (t.getWidth() + 10);
			}
			scn.add(t);
		}
		
		int height = 10 + (15 + 2) * i;
		DrawUtil.setCanvasSize(content,width,height);
		DrawUtil.drawRoundRect(content.getContext2d(), 0, 0, width, height, 5, "white", "darkgrey");
		scn.draw();
		
		Window win = new Window("About Logicamo", content);
		win.setSize(width, height);
		return win;
	}
	
	/**
	 * Creates the menu-bar for the application
	 * 
	 * @param cmd
	 *            placeholder command for the menu-functions
	 */
	private void createMenuBar() {
		MenuBar menubar = new MenuBar(false);
		MenuBar filebar = new MenuBar(true);
		MenuBar editbar = new MenuBar(true);
		MenuBar helpbar = new MenuBar(true);

		Command cmd = new Command() {
			@Override
			public void execute() {
				Logger.log(this, "Generic command executed");
			}
		};

		Command newCircuit = new Command() {
			private native void reloadPage() /*-{
				$doc.location.reload();
			}-*/;

			@Override
			public void execute() {
				Logger.log(this,"Reload page");
				reloadPage();
			}
		};

		Command printCircuit = new Command() {
			private native void print(Element canvasElement) /*-{
				var image = $doc.createElement('img');
				image.src = canvasElement.toDataURL();
		
				// paperwidth A4 screen width
				var paperWidth = 842;
		
				var imagewidth = image.width;
				var imageheight = image.height;
		
				if (image.width > paperWidth) {
					var zooming = image.width / paperWidth;
					imagewidth = image.width / zooming;
					imageheight = image.height / zooming;
				}
		
				image.style.width = imagewidth + "px";
				image.style.height = imageheight + "px";
		
				var printArea = $doc.getElementById("printcontent").contentWindow;
		
				printArea.document.open();
		
				printArea.document.innerHTML = '';
				printArea.document.appendChild(image);
				printArea.document.close();
				printArea.focus();
				printArea.print();
		
			}-*/;

			@Override
			public void execute() {
				print(Globals.mainEngine.getCanvas().getElement());
				Logger.log(this, "printing dialog for circuit opened");
			}
		};

		Command manual = new Command() {
			@Override
			public void execute() {
				Logger.log(this, "manual command executed");
			}
		};

		Command aboutLogicamo = new Command() {
			Window aboutPopup = null;
			
			@Override
			public void execute() {
				Logger.log(this, "Open \"about Logicamo\" popup");
				if(aboutPopup == null) {
					aboutPopup = createAboutBox(
							  "Logicamo - an educational tool for logic circuits\n" 
							+ "\n"
							+ "Logicamo started as a Master's thesis project by Sara Seppola\n"
							+ "at Åbo Akademi University in January 2013.\n"
							+ "\n"
							+ "Programming: Sara Seppola\n"
							+ "Graphics: Kasper Lindström\n"
							+ "Additional code: Patrik Lindström\n"
							+ "\n");
				}
				
				Globals.mainloop.getWorkspace().addPopup(aboutPopup);
			}
		};

		MenuItem saveItem = new MenuItem("Save Circuit", cmd);
		saveItem.setEnabled(false);

		MenuItem editItem = new MenuItem("Edit", editbar);
		editItem.setEnabled(false);

		MenuItem manualItem = new MenuItem("Manual", manual);
		manualItem.setEnabled(false);

		MenuItem aboutItem = new MenuItem("About Logicamo", aboutLogicamo);

		filebar.addItem("New Circuit", newCircuit);
		filebar.addItem(saveItem);
		filebar.addItem("Print Circuit", printCircuit);
		helpbar.addItem(manualItem);
		helpbar.addItem(aboutItem);

		menubar.addItem("File", filebar);
		menubar.addSeparator();
		menubar.addItem(editItem);
		menubar.addSeparator();
		menubar.addItem("Help", helpbar);

		RootPanel.get().add(menubar);
	}

}
