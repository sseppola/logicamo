package com.abo.logicamo.client.workspace;

import java.util.ArrayList;

import com.abo.logicamo.client.Globals;
import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.graphics.Engine;
import com.abo.logicamo.client.graphics.Engine.ResizeHandler;
import com.abo.logicamo.client.graphics.Node;
import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.nodes.Button;
import com.abo.logicamo.client.graphics.nodes.Button.ButtonHandler;
import com.abo.logicamo.client.graphics.nodes.Rectangle;
import com.abo.logicamo.client.graphics.nodes.Window;
import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;
import com.abo.logicamo.client.logic.InputConnector;
import com.abo.logicamo.client.logic.OutputConnector;
import com.abo.logicamo.client.logic.Wire;
import com.abo.logicamo.client.logic.gates.InputGate;
import com.abo.logicamo.client.logic.gates.OutputGate;

public class Workspace {

	private Engine engine;

	/**
	 * The root node on the canvas. Contains the scene, the background and the
	 * foreground.
	 */
	private Node canvasNode;

	/**
	 * Contains the gates, the wires and the popups
	 */
	private Node scene;

	/**
	 * Contains the background behind the gates
	 */
	private Node background;

	/**
	 * This might be needed in the future. Possible the popups will move here.
	 */
	private Node foreground;

	/**
	 * A layer in the background where the input gates are located
	 */
	private Rectangle inputBackground;

	/**
	 * A layer in the background where the output gates are located
	 */
	private Rectangle outputBackground;

	/**
	 * Creates a new input gate
	 */
	private Button addNewInput;

	/**
	 * Creates a new output gate
	 */
	private Button addNewOutput;

	/**
	 * The horizontal location for the topmost input and output gates
	 */
	private static final double ioGateStartY = 100;

	// Lists for all the objects that are added to the canvas
	private ArrayList<InputGate> inputGates;
	private ArrayList<OutputGate> outputGates;
	private ArrayList<Wire> wires;
	private ArrayList<Wire> wiresToRemove;
	private ArrayList<Gate> gates;
	private ArrayList<Gate> gatesToRemove;

	public Workspace(Engine e) {

		Logger.log(this, "entered constructor");

		// List initialization
		gates = new ArrayList<Gate>();
		wires = new ArrayList<Wire>();
		inputGates = new ArrayList<InputGate>();
		outputGates = new ArrayList<OutputGate>();
		gatesToRemove = new ArrayList<Gate>();
		wiresToRemove = new ArrayList<Wire>();

		// Scene roots
		engine = e;
		scene = new Node();
		canvasNode = new Node();
		background = new Node();
		foreground = new Node();

		// Inputbackground initialization
		inputBackground = new Rectangle(false);
		inputBackground.setPosition(0, 0);
		inputBackground.setSize(100, engine.getScreenHeight());
		inputBackground.setColour(Globals.inputbar_col);

		// Outputbackground initialization
		outputBackground = new Rectangle(false);
		outputBackground.setSize(100, engine.getScreenHeight());
		outputBackground.setPosition(engine.getScreenWidth() - outputBackground.getWidth(), 0);
		outputBackground.setColour(Globals.inputbar_col);

		// Create "add input" button
		addNewInput = new Button(Loader.getImageById("input_gate_button"), new ButtonHandler() {
			@Override
			public void onRelease(Button b) {
			}

			@Override
			public void onPress(Button b) {
			}

			@Override
			public void onClick(Button b) {
				InputGate i = new InputGate();
				int inputSize = inputGates.size();
				double nextYPos = i.getHeight() + 10;

				if (inputSize > 0) {

					int j = 0, l;
					for (j = 0, l = inputGates.size(); j < l; j++) {
						inputGates.get(j).setPosition(inputBackground.getWidth() - i.getWidth(),
								ioGateStartY + (j * nextYPos));
					}
					i.setPosition(inputBackground.getWidth() - i.getWidth(), ioGateStartY + (j * nextYPos));
				} else {
					i.setPosition(inputBackground.getWidth() - i.getWidth(), ioGateStartY);
				}
				inputGates.add(i);
				background.add(i);
				addNewInput.setPosition(i.getX(), i.getY() + nextYPos);
			}
		});
		addNewInput.setPosition(inputBackground.getWidth() - addNewInput.getWidth(),
				ioGateStartY + addNewInput.getHeight() + 10);

		// Create "add output" button
		addNewOutput = new Button(Loader.getImageById("output_gate_button"), new ButtonHandler() {

			@Override
			public void onRelease(Button b) {
			}

			@Override
			public void onPress(Button b) {
			}

			@Override
			public void onClick(Button b) {
				OutputGate o = new OutputGate();
				int outputSize = outputGates.size();
				double nextYPos = o.getHeight() + 10;

				if (outputSize > 0) {

					int j = 0, l;
					for (j = 0, l = outputGates.size(); j < l; j++) {
						outputGates.get(j).setPosition(outputBackground.getX(), ioGateStartY + (j * nextYPos));
					}
					o.setPosition(outputBackground.getX(), ioGateStartY + (j * nextYPos));
				} else {
					o.setPosition(outputBackground.getX(), ioGateStartY);
				}
				outputGates.add(o);
				background.add(o);
				addNewOutput.setPosition(o.getX(), o.getY() + nextYPos);

			}
		});
		addNewOutput.setPosition(outputBackground.getX(), ioGateStartY + addNewOutput.getHeight() + 10);

		// Create initial input gate
		InputGate firstInput = new InputGate();
		firstInput.setPosition(inputBackground.getWidth() - firstInput.getWidth(), ioGateStartY);
		inputGates.add(firstInput);

		// Create initial output gate
		OutputGate firstOutput = new OutputGate();
		firstOutput.setPosition(outputBackground.getX(), ioGateStartY);
		outputGates.add(firstOutput);

		// Place canvas below gate bar
		canvasNode.setPosition(0, Gatebar.DEFAULT_HEIGHT);

		// Set up scene graph
		canvasNode.add(background);
		canvasNode.add(scene);
		canvasNode.add(foreground);

		background.add(inputBackground);
		background.add(outputBackground);
		background.add(firstInput);
		background.add(addNewInput);
		background.add(firstOutput);
		background.add(addNewOutput);

		// Add root node to engine
		engine.add(canvasNode);

		// Add screen resize handler
		engine.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(int newWidth, int newHeight) {
				outputBackground.setSize(100, newHeight);
				outputBackground.setPosition(newWidth - outputBackground.getWidth(), 0);

				inputBackground.setSize(100, newHeight);
				
				// Adjust output gate positions...
				for(OutputGate g : outputGates) {
					g.setPosition(outputBackground.getX(), g.getY());
				}
				
				// Move "add new output" button
				addNewOutput.setPosition(outputBackground.getX(), addNewOutput.getY());

			}
		});

		Logger.log(this, "constructor done");
	}

	/**
	 * All changes in the positioning and logic in the circuit are processed
	 * here as well as additions or removals of gates, wires or popups
	 */
	public void update() {

		// Process gate logic and update positions:
		// con_selection will define if a wire is created when dragging from a
		// input- or outputconnector
		// this has to be checked from all the gates (normal, inputgates and
		// outputgates)
		boolean con_selection = false;
		for (Gate g : gates) {
			g.updateLogic(); // every third frame?
			con_selection = g.updateMouse() || con_selection;
		}
		for (InputGate g : inputGates) {
			g.updateLogic();
			con_selection = g.updateMouse() || con_selection;
		}
		for (OutputGate g : outputGates) {
			g.updateLogic();
			con_selection = g.updateMouse() || con_selection;
		}

		if (!con_selection) {
			Globals.selectedConnector = null;
		}

		// Update wires
		for (Wire w : wires) {
			w.update();
		}

		// Remove all gates that are deleted by user
		for (Gate g : gatesToRemove) {
			if (g instanceof InputGate) {
				removeInput((InputGate) g);
			} else if (g instanceof OutputGate) {
				removeOutput((OutputGate) g);
			} else {
				removeGate(g);
			}
		}
		gatesToRemove.clear();

		for (Wire w : wiresToRemove) {
			removeWire(w);
		}
		wiresToRemove.clear();
	}

	public void addToGates(Gate g) {
		gates.add(g);
	}

	public void addToWires(Wire w) {
		wires.add(w);
	}

	public void addPopup(Window p) {
		foreground.add(p);
	}

	public void addToScene(Node n) {
		scene.add(n);
	}

	public void queueGateForRemoval(Gate g) {
		gatesToRemove.add(g);
	}

	public void queueWireForRemoval(Wire w) {
		wiresToRemove.add(w);
	}

	public ArrayList<InputGate> getInputGates() {
		return inputGates;
	}

	public ArrayList<OutputGate> getOutputGates() {
		return outputGates;
	}

	private void removeInput(InputGate i) {

		for (Wire w : wires) {
			if (w.getFromGate() == i) {
				wiresToRemove.add(w);
			}
		}
		i.setState(GateValue.UNDEFINED); // important so that the following
											// gates states are updated
		inputGates.remove(i);

		double nextYPos = i.getHeight() + 10;

		int j = 0, l;
		for (j = 0, l = inputGates.size(); j < l; j++) {
			inputGates.get(j).setPosition(inputBackground.getWidth() - i.getWidth(), ioGateStartY + (j * nextYPos));
		}

		addNewInput.setPosition(inputBackground.getWidth() - i.getWidth(), ioGateStartY + (j * nextYPos));
		background.remove(i);
	}

	private void removeOutput(OutputGate o) {
		for (Wire w : wires) {
			if (w.getToGate() == o) {
				wiresToRemove.add(w);
			}
		}
		outputGates.remove(o);

		double nextYPos = o.getHeight() + 10;

		int j = 0, l;
		for (j = 0, l = outputGates.size(); j < l; j++) {
			outputGates.get(j).setPosition(outputBackground.getX(), ioGateStartY + (j * nextYPos));
		}

		addNewOutput.setPosition(outputBackground.getX(), ioGateStartY + (j * nextYPos));
		background.remove(o);
	}

	private void removeGate(Gate gate) {

		// Remove all connections to this gate from other gates
		for (Gate g : gates) {
			if (g.isConnectedTo(gate)) {
				g.removeConnection(gate);
			} else if (gate.isConnectedTo(g)) {
				g.setState(GateValue.UNDEFINED);
			}
		}
		for (InputGate i : inputGates) {
			if (i.isConnectedTo(gate)) {
				i.removeConnection(gate);
			}
		}
		// Remove wires connected to this gate
		for (Wire w : wires) {
			if ((w.getFromGate() == gate) || (w.getToGate() == gate)) {
				wiresToRemove.add(w);
			}
		}

		gate.setState(GateValue.UNDEFINED); // important so that the following
											// gates states are updated
		gates.remove(gate);
		gate.removeFromParent();

	}

	private void removeWire(Wire w) {
		w.removeFromParent();
		InputConnector tocon = w.getToCon();
		tocon.setConnection(false);
		tocon.removeWire();
		OutputConnector fromcon = w.getFromCon();
		fromcon.getWires().remove(w);
		wires.remove(w);
	}
}
