package com.abo.logicamo.client.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.abo.logicamo.client.Globals;
import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.graphics.Engine;
import com.abo.logicamo.client.graphics.Engine.ResizeHandler;
import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.nodes.Button;
import com.abo.logicamo.client.graphics.nodes.Button.ButtonHandler;
import com.abo.logicamo.client.graphics.nodes.Rectangle;
import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.gates.AndGate;
import com.abo.logicamo.client.logic.gates.NandGate;
import com.abo.logicamo.client.logic.gates.NorGate;
import com.abo.logicamo.client.logic.gates.NotGate;
import com.abo.logicamo.client.logic.gates.OrGate;
import com.abo.logicamo.client.logic.gates.XnorGate;
import com.abo.logicamo.client.logic.gates.XorGate;

/**
 * The area below menu bar. Shows the gates that can be added to the canvas.
 * When you press upon a picture of a gate, the gate is created and added to the
 * canvas.
 * 
 * @author Sara Seppola
 */
public class Gatebar {
	
	public static final int DEFAULT_HEIGHT = 100;
	
	private Engine engine;
	private Workspace workspace;
	private ArrayList<Button> gateCreatorButtons;
	private Map<Button, Gate> buttonToGateMap;

	public Gatebar(Engine e) {

		Logger.log(this, "In constructor");
		engine = e;

		gateCreatorButtons = new ArrayList<Button>();

		final Rectangle gatebar = new Rectangle(false);
		gatebar.setColour(Globals.gatebar_col);
		gatebar.setSize(engine.getScreenWidth(), DEFAULT_HEIGHT);

		buttonToGateMap = new HashMap<Button, Gate>();

		//
		// Create gate creator buttons
		//
	
		addCreatorButton("gate_and_button", new AndGate());
		addCreatorButton("gate_or_button", new OrGate());
		addCreatorButton("gate_not_button", new NotGate());
		addCreatorButton("gate_nand_button", new NandGate());
		addCreatorButton("gate_nor_button", new NorGate());
		addCreatorButton("gate_xor_button", new XorGate());
		addCreatorButton("gate_xnor_button", new XnorGate());

		// Position the newly created buttons
		double bx = 10;
		int i, l;
		for (i = 0, l = gateCreatorButtons.size(); i < l; ++i) {
			Button b = gateCreatorButtons.get(i);
			b.setPosition(bx, (gatebar.getHeight() - b.getHeight()) * 0.5);
			// (y equals Half of available max height minus half of the sprite's
			// height)
			bx += b.getWidth() + 10;
			gatebar.add(b);
		}

		engine.add(gatebar);

		engine.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(int newWidth, int newHeight) {
				gatebar.setSize(newWidth, DEFAULT_HEIGHT);
			}
		});
		
		Logger.log(this, "Created.");
	}
	
	private ButtonHandler buttonHandler = new ButtonHandler() {
		@Override
		public void onRelease(Button b) {
			Gate currentGate = buttonToGateMap.get(b);
			currentGate.stopDrag();
			
			if (currentGate.getY() > 0) {
				Gate g = currentGate.makeClone();
				g.setPosition(currentGate.getX(), currentGate.getY());

				workspace.addToGates(g);
				workspace.addToScene(g);
			}

			currentGate.removeFromParent();
		}

		@Override
		public void onPress(Button b) {
			Gate g = buttonToGateMap.get(b);
			
			double middleX = g.getWidth() / 2.0;
			double middleY = g.getHeight() / 2.0;

			g.setPosition(b.getX() + b.getMouseX() - middleX,
					b.getY() + b.getMouseY() - DEFAULT_HEIGHT - middleY);				
			
			workspace.addToScene(g);
			g.startDrag();
		}

		@Override
		public void onClick(Button b) {
		}
	};

	private void addCreatorButton(String buttonImageID, Gate g) {
		Button b = new Button(Loader.getImageById(buttonImageID), buttonHandler);
		gateCreatorButtons.add(b);
		buttonToGateMap.put(b, g);
		g.setOpacity(0.5);
	}

	public void setWorkspace(Workspace w) {
		workspace = w;
	}
}
