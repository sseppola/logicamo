package com.abo.logicamo.client;

import com.abo.logicamo.client.graphics.Engine;
import com.abo.logicamo.client.logic.Connector;

public class Globals {

	public static Engine mainEngine;
	public static Connector selectedConnector;
	public static MainLoop mainloop;

	// Color definitions
	public static final String background_col = "white";
	public static final String inputbar_col = "#bbaaaa";
	public static final String inputbar_text_col = "#eedddd";
	public static final String gatebar_col = "#fff7f7";
	public static final String clear_col = "#ffeeee";

}
