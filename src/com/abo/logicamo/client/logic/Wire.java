package com.abo.logicamo.client.logic;

import com.abo.logicamo.client.graphics.Node;
import com.abo.logicamo.client.graphics.nodes.Sprite;
import com.google.gwt.canvas.dom.client.Context2d;

public class Wire extends Node {

	private Sprite from;
	private Sprite to;

	private Gate fromGate;
	private Gate toGate;

	private OutputConnector fromCon;
	private InputConnector toCon;

	private double toX;
	private double toY;

	private int state;
	private boolean dragged;
	private boolean inputdrag;

	private String colour;
	private double bX1; // bezier curve control points
	private double bY1;
	private double bX2;
	private double bY2;

	public Wire(OutputConnector from, InputConnector to, Gate fromGate,
			Gate toGate) {

		setSize(5, 0);

		state = GateValue.UNDEFINED;
		toX = 0;
		toY = 0;
		colour = "black";

		fromCon = from;
		toCon = to;

		if ((from != null) && (to != null) && (fromGate != null)
				&& (toGate != null)) {
			this.from = from.getSprite();
			this.to = to.getSprite();
			this.fromGate = fromGate;
			this.toGate = toGate;
			dragged = false;
		} else if ((from == null) && (fromGate == null)) {
			this.to = to.getSprite();
			dragged = true;
			inputdrag = true;
		} else {
			this.from = from.getSprite();
			dragged = true;
			inputdrag = false;
		}
	}

	public void draw(Context2d ctx) {
		ctx.beginPath();
		ctx.setStrokeStyle(colour);
		ctx.setLineWidth(getWidth());
		ctx.moveTo(0, 0);
		ctx.bezierCurveTo(bX1, bY1, bX2, bY2, toX, toY);
		ctx.stroke();
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}

	/**
	 * called from the Connector-classes
	 */
	public void update() {
		if (dragged) {
			updateDragWire();
		} else {
			updateNormalWire();
			// TODO: check mouse

		}
		fixBezier();
	}

	/**
	 * A normal wires position is calculated based on the position of the
	 * connectors it is connected to. Its colour is defined by possible
	 * electricity flow in the gate.
	 */
	private void updateNormalWire() {
		double x = from.getParent().getX() + from.getX() + from.getWidth();
		double y = from.getParent().getY() + from.getY()
				+ (0.5 * from.getHeight());

		toX = (to.getParent().getX() + to.getX()) - x;
		toY = (to.getParent().getY() + to.getY() + (0.5 * to.getHeight())) - y;

		setPosition(x, y);

		state = fromGate.getOutput().getState();
		toCon.setState(state);

		if (state == 0) {
			setColour("grey");
		} else if (state == 1) {
			setColour("black");
		} else {
			// electricity
			setColour("#a90404");
		}
	}

	/**
	 * The dragwires position is calculated based on if its drawn from an input-
	 * or an outputconnector
	 */
	private void updateDragWire() {
		if (inputdrag) {
			double xoffs = to.getX();
			double yoffs = to.getY() + (0.5 * to.getHeight());

			double x = getParent().getMouseX();
			double y = getParent().getMouseY();

			toX = (to.getParent().getX() + xoffs) - x;
			toY = (to.getParent().getY() + yoffs) - y;

			setPosition(x, y);

			setColour("grey");
			setOpacity(0.6);
		} else {
			double xoffs = from.getX() + from.getWidth();
			double yoffs = from.getY() + (0.5 * from.getHeight());

			double x = from.getParent().getX() + xoffs;
			double y = from.getParent().getY() + yoffs;

			toX = getParent().getMouseX() - x;
			toY = getParent().getMouseY() - y;

			setPosition(x, y);

			setColour("grey");
			setOpacity(0.6);
		}
	}

	public void fixBezier() {
		if (toX >= 0) {
			bX1 = toX / 3;
			bX2 = bX1 * 2;
			bY1 = 0;
			bY2 = toY;
		} else {
			bX1 = -(toX / 3);
			bX2 = toX - bX1;
			bY1 = 0;
			bY2 = toY;
		}
	}

	public OutputConnector getFromCon() {
		return fromCon;
	}

	public InputConnector getToCon() {
		return toCon;
	}

	public Gate getFromGate() {
		return fromGate;
	}

	public Gate getToGate() {
		return toGate;
	}

	public Sprite getFromSprite() {
		return from;
	}

	public Sprite getToSprite() {
		return to;
	}
}
