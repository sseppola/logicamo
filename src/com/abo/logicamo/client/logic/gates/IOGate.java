package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.nodes.Text;
import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;
import com.google.gwt.user.client.ui.Image;

public abstract class IOGate extends Gate {

	private Image image_null;
	private Image image_zero;
	private Image image_one;
	private Text nameText;
	
	private int value;
	
	protected IOGate(String image_null_id, String image_zero_id, String image_one_id, String name, int inputs, boolean text_left) {
		super(image_null_id, name, inputs);
		
		image_null = Loader.getImageById(image_null_id);
		image_zero = Loader.getImageById(image_zero_id);
		image_one = Loader.getImageById(image_one_id);
		
		nameText = new Text(name);
		if(text_left) {
			nameText.setPosition(-20, (image_null.getHeight() / 2) + 10);
		} else {
			nameText.setPosition(getWidth() + 5, (image_null.getHeight() / 2) + 10);
		}
		add(nameText);
		
		value = 0;
		
		disableDragging();
	}
	
	@Override
	public final boolean process(boolean[] inputs) {
		return true;
	}
	
	public int getValue() {
		return value;
	}
	
	protected void setValue(int value) {
		this.value = value;
		setState(value);
		updateSprite();
	}

	private void update() {
		setState(value);
		updateSprite();
	}
	
	private void updateSprite() {
		switch(value) {
		case GateValue.UNDEFINED:
			setImage(image_null);
		break;
		case GateValue.FALSE:
			setImage(image_zero);
		break;
		case GateValue.TRUE:
			setImage(image_one);
		break;
		default:
			Logger.warn(this, "Undefined gate value " + value);
		break;
		}
	}
	
	@Override
	public boolean updateMouse() {
		boolean value = super.updateMouse();
		update();
		return value;
	}
	
}
