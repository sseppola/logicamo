package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.logic.Gate;

/**
 * Bill is needed when evaluating a circuit that contains loops. The loop is cut
 * open from somewhere, and the open wire is connected to a placeholder-gate
 * Bill. Thus we get an expression of type (A and B) and b0, where all the
 * variables bi mark a loop in the circuit, instead of an infinite expression of
 * this type: (A and B)i and (A and B)i' and (A and B)i'' and (A and B)i''' and
 * ....
 * 
 * @author Sara
 *
 */
public class BillGate extends Gate {

	private static int counter;

	/**
	 * Bill Gate returns always true. Bill Gate is used as a dumb gate type in
	 * the logic tree
	 */
	public BillGate() {
		super(null, "b" + counter++, 2);
	}

	@Override
	public boolean process(boolean[] inputs) {
		return true;
	}

	@Override
	@SuppressWarnings("unchecked")
	public BillGate makeClone() {
		return new BillGate();
	}

}
