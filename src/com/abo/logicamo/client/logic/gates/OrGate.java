package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;

public class OrGate extends Gate {

	public OrGate() {
		super("gate_or", GateValue.OR, 2);
		setInputXOffset(25);
	}

	/**
	 * OR-gate returns true if any of the inputs are true.
	 */
	@Override
	public boolean process(boolean[] inputs) {

		// If any of the inputs are true, OR-gate returns true.
		for (int i = 0; i < inputs.length; i++) {
			if (inputs[i] == true) {
				setState(GateValue.TRUE);
				return true;
			}
		}
		setState(GateValue.FALSE);
		return false;
	}

	public void processUndefined(int[] inputs) {
		int length = inputs.length;
		for (int i = 0; i < length; i++) {
			if (inputs[i] == GateValue.TRUE) {
				setState(GateValue.TRUE);
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public OrGate makeClone() {
		return new OrGate();
	}
}
