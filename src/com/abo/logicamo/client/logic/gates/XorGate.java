package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;

public class XorGate extends Gate {

	public XorGate() {
		super("gate_xor", GateValue.XOR, 2);
		setInputXOffset(28);
	}

	/**
	 * XOR-gate returns true if the two inputs have different values
	 */
	@Override
	public boolean process(boolean[] inputs) {

		if (inputs[0] != inputs[1]) {
			setState(GateValue.TRUE);
			return true;
		}

		setState(GateValue.FALSE);
		return false;
	}

	@Override
	@SuppressWarnings("unchecked")
	public XorGate makeClone() {
		return new XorGate();
	}
}
