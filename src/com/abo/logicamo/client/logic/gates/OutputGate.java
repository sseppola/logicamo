package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.Globals;
import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.logic.InputConnector;
import com.abo.logicamo.client.logic.LogicTree;
import com.abo.logicamo.client.logic.TruthTableWindow;

public class OutputGate extends IOGate {

	private final InputConnector input;

	static int counter = 1;
	boolean showexpression;

	private static int POPUP_LOCATION = 0;

	public OutputGate() {
		super("output_gate", "output_gate_zero", "output_gate_one", "Q" + counter, 1, false);

		// Hide the output sprite
		getOutput().getSprite().removeFromParent();

		input = getInput(0);
		showexpression = false;
		counter++;
	}

	@Override
	public boolean updateMouse() {
		boolean retval = super.updateMouse();

		setValue(input.getState());

		// The OutputGate is clicked on
		if (isMouseOver() && isMousePressed()) {
			handlePopup();
		}

		return retval;
	}

	private void handlePopup() {
		TruthTableWindow popup = null;

		// create and show logic expression
		Logger.log(this, "trying to make a logic tree and expression");
		LogicTree tree = new LogicTree(this);
		Logger.log(this, "a logic tree was created");
		String expression = tree.getExpressions()[0];
		Logger.log(this, "a logic expression was created");

		if (expression.contains("ERROR")) {
			Logger.log(this, "a logic tree could not be created, since the tree is not correct");
		} else {
			popup = new TruthTableWindow(expression, this);

			popup.setPosition(200 + POPUP_LOCATION, 200 + POPUP_LOCATION);
			POPUP_LOCATION += 10;
			if (POPUP_LOCATION >= 200) {
				POPUP_LOCATION = 0;
			}
		}

		if (popup != null) {
			Globals.mainloop.getWorkspace().addPopup(popup);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public OutputGate makeClone() {
		return new OutputGate();
	}
}
