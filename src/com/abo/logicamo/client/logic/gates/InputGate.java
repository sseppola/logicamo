package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.nodes.Button;
import com.abo.logicamo.client.graphics.nodes.Button.ButtonHandler;

/**
 * Models the gate feeding electricity INTO the circuit.
 * These gates can be toggled between ZERO and ONE states
 * by clicking on them, and returned to NULL state by
 * clicking the 'clear' button next to them. 
 */
public class InputGate extends IOGate {

	private final Button clearButton;
	private static int counter = 65;

	public InputGate() {

		/*
		 * The original code was as follows.. It's been inlined into the super
		 * call
		 * 
		 * 
		 * if (counter > 100) { int i = counter / 100; name =
		 * Character.toString((char) (counter % 100)) + i; } else { name =
		 * Character.toString((char) counter); }
		 */

		super("input_gate", "input_gate_zero", "input_gate_one",
				Character.toString((char) ((counter > 100) ? ((counter % 100) + (counter / 100)) : counter)), 
				0, true);

		counter++;
		if (counter == 91) {
			counter = counter + 74;
		} else if (counter == 81) {
			// no "Q" will be allowed
			counter++;
		}

		clearButton = new Button(Loader.getImageById("button_clear"), new ButtonHandler() {
			@Override
			public void onRelease(Button b) {
			}

			@Override
			public void onPress(Button b) {
			}

			@Override
			public void onClick(Button b) {
				setValue(0);
			}
		});

		double half = clearButton.getWidth() / 2;
		clearButton.setPosition(-half, half);
		add(clearButton);
	}

	@Override
	public boolean updateMouse() {
		
		boolean value = super.updateMouse();

		// Mouse clicks toggle state
		// Null and false become true
		// true becomes false
		if (isMouseOver() && isMousePressed()) {
			switch(getValue()) {
			case 0:
			case 2:
				setValue(1);
			break;
			case 1:
				setValue(2);
			break;
			}
		}
		
		if (clearButton.isMouseOver()) {
			clearButton.setOpacity(1.0);
		} else {
			clearButton.setOpacity(0.0);
		}

		return value;
	}

	@Override
	@SuppressWarnings("unchecked")
	public InputGate makeClone() {
		return new InputGate();
	}

}
