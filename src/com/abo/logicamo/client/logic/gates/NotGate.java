package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;

public class NotGate extends Gate {

	public NotGate() {
		super("gate_not", GateValue.NOT, 1);
	}

	/**
	 * NOT-gate returns the inverse of the input.
	 */
	@Override
	public boolean process(boolean[] inputs) {

		// Since NOT-gates only have one input-value, we don't need to check for
		// input array size.
		// Returns the inverse of the input-value.
		if (inputs[0] == false) {
			setState(GateValue.TRUE);
		} else {
			setState(GateValue.FALSE);
		}
		return !inputs[0];
	}

	@Override
	@SuppressWarnings("unchecked")
	public NotGate makeClone() {
		return new NotGate();
	}

}
