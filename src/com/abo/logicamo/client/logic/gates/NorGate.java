package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;

public class NorGate extends Gate {

	public NorGate() {
		super("gate_nor", GateValue.NOR, 2);
		setInputXOffset(25);
	}

	/**
	 * NOR-gate returns true if all the inputs are false
	 */
	@Override
	public boolean process(boolean[] inputs) {

		for (int i = 0; i < inputs.length; i++) {
			if (inputs[i] == true) {
				setState(GateValue.FALSE);
				return false;
			}
		}
		setState(GateValue.TRUE);
		return true;
	}

	public void processUndefined(int[] inputs) {
		int length = inputs.length;
		for (int i = 0; i < length; i++) {
			if (inputs[i] == GateValue.TRUE) {
				setState(GateValue.FALSE);
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public NorGate makeClone() {
		return new NorGate();
	}
}
