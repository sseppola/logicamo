package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;

public class XnorGate extends Gate {

	public XnorGate() {
		super("gate_xnor", GateValue.XNOR, 2);
		setInputXOffset(28);
	}

	/**
	 * XNOR-gate returns true if the two inputs have the same value
	 */
	@Override
	public boolean process(boolean[] inputs) {

		if (inputs[0] == inputs[1]) {
			setState(GateValue.TRUE);
			return true;
		}

		setState(GateValue.FALSE);
		return false;
	}

	@Override
	@SuppressWarnings("unchecked")
	public XnorGate makeClone() {
		return new XnorGate();
	}
}
