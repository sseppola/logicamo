package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;

public class AndGate extends Gate {

	public AndGate() {
		super("gate_and", GateValue.AND, 2);
	}

	/**
	 * Returns true only if all the inputs of the AND-gate are true.
	 */
	@Override
	public boolean process(boolean[] inputs) {
		// Returns true if all the inputs are true.
		for (int i = 0; i < inputs.length; i++) {
			if (inputs[i] == false) {
				setState(GateValue.FALSE);
				return false;
			}
		}
		setState(GateValue.TRUE);
		return true;
	}

	public void processUndefined(int[] inputs) {
		int length = inputs.length;
		for (int i = 0; i < length; i++) {
			if (inputs[i] == GateValue.FALSE) {
				setState(GateValue.FALSE);
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public AndGate makeClone() {
		return new AndGate();
	}

}
