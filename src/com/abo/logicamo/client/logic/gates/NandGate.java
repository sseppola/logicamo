package com.abo.logicamo.client.logic.gates;

import com.abo.logicamo.client.logic.Gate;
import com.abo.logicamo.client.logic.GateValue;

public class NandGate extends Gate {

	public NandGate() {
		super("gate_nand", GateValue.NAND, 2);
	}

	/**
	 * Returns true only if at least one of the inputs in NAND-gate is false
	 */
	@Override
	public boolean process(boolean[] inputs) {

		for (int i = 0; i < inputs.length; i++) {
			if (inputs[i] == false) {
				setState(GateValue.TRUE);
				return true;
			}
		}
		setState(GateValue.FALSE);
		return false;
	}

	public void processUndefined(int[] inputs) {
		int length = inputs.length;
		for (int i = 0; i < length; i++) {
			if (inputs[i] == GateValue.FALSE) {
				setState(GateValue.TRUE);
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public NandGate makeClone() {
		return new NandGate();
	}

}
