package com.abo.logicamo.client.logic;

import com.abo.logicamo.client.Globals;
import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.nodes.Button;
import com.abo.logicamo.client.graphics.nodes.Button.ButtonHandler;
import com.abo.logicamo.client.graphics.nodes.Sprite;

public class InputConnector extends Connector {

	private boolean hasConnection;

	private Wire wire;
	private Wire dragWire;

	public InputConnector(boolean flatSide) {

		wire = null;
		hasConnection = false;
		isActive = false;
		selected = false;
		state = GateValue.UNDEFINED;

		if (flatSide) {
			if (active_image == null) {
				active_image = Loader.getImageById("connector_back_active");
			}
			if (normal_image == null) {
				normal_image = Loader.getImageById("connector_back_inactive");
			}
		} else {
			if (active_image == null) {
				active_image = Loader.getImageById("connector_active");
			}
			if (normal_image == null) {
				normal_image = Loader.getImageById("connector_inactive");
			}
		}
		sprite = new Sprite(normal_image);
		sprite.setPickingEnabled(true);

		remover = new Button(Loader.getImageById("button_remove"), new ButtonHandler() {
			@Override
			public void onRelease(Button b) {
			}

			@Override
			public void onPress(Button b) {
			}

			@Override
			public void onClick(Button b) {
				Globals.mainloop.getWorkspace().queueWireForRemoval(getWire());
				Logger.log(this, "wire added for removal");
			}
		});

		remover.setPosition(-remover.getWidth(), -remover.getWidth());
		remover.setOpacity(0.0);

		sprite.add(remover);
	}

	public void setConnection(boolean connection) {
		hasConnection = connection;
		if (!hasConnection) {
			setState(GateValue.UNDEFINED);
		}
	}

	public boolean hasConnection() {
		return hasConnection;
	}

	@Override
	public void update() {

		updatePictureAndActivity();
		updateMouse();
	}

	/**
	 * Create or remove dragwires depending on mouse position and clicks.
	 */
	private void updateMouse() {

		updateOrRemoveDragWire();

		if (!selected) {

			if (hasConnection()
					&& ((!sprite.isMouseDown() && isActive) || remover
							.isMouseOver())) {
				remover.setOpacity(1.0);
			} else {
				remover.setOpacity(0.0);
			}

			if (isActive) {
				if (sprite.isMousePressed()) {
					if (!hasConnection) {
						startDragWire();
					}
				} else if (sprite.isMouseReleased()) {
					if ((Globals.selectedConnector != null)
							&& (Globals.selectedConnector instanceof OutputConnector)
							&& !hasConnection) {
						// We have a wire that has been started from an
						// outputconnector and our inputconnector is not yet
						// connected

						createAndSaveWire();
					} else {
						Logger.log(this,
								"input connector might already be connected");
					}
				}
			}
		} else {
			if (sprite.isMouseReleased()) {
				selected = false;
			}
		}
	}

	/**
	 * An active dragwire is updated or removed
	 */
	private void updateOrRemoveDragWire() {
		if (dragWire != null) {
			dragWire.update();

			if (Globals.mainEngine.isMouseUp()) { // TODO: could this be also
													// checked by sprite instead
													// of engine?
				dragWire.removeFromParent();
				dragWire = null;
			}
		}
	}

	/**
	 * A dragwire has been connected to this inputconnector from an
	 * outputconnector. A new wire is now created between the two connectors and
	 * saved in workspace's wirelist.
	 */
	private void createAndSaveWire() {

		// create a line between cons
		OutputConnector outputcon = (OutputConnector) Globals.selectedConnector;
		Wire w = new Wire(outputcon, this, outputcon.parent, parent);
		wire = w;
		outputcon.getWires().add(w);
		hasConnection = true;
		Globals.mainloop.getWorkspace().addToWires(w); // TODO: GET RID OF THIS
														// SPAGHETTI!!
		outputcon.getParent().connectTo(parent);
		Globals.mainloop.getWorkspace().addToScene(w);
	}

	/**
	 * A wire is created when mousepressing and dragging on the inputconnector
	 */
	private void startDragWire() {

		selected = true;
		dragWire = new Wire(null, this, null, parent);
		Globals.mainloop.getWorkspace().addToScene(dragWire);
	}

	/**
	 * The connector picture is switched between active and non-active depending
	 * on mouse position
	 */
	private void updatePictureAndActivity() {

		// check if is active or/and selected
		if (!isActive && sprite.isMouseOver()) {
			sprite.setImage(active_image);
			isActive = true;

		}
		if (isActive && !sprite.isMouseOver()) {
			if (!selected) {
				sprite.setImage(normal_image);
				isActive = false;
			}
		}
	}

	public void removeWire() {
		wire = null;
	}

	public Wire getWire() {
		return wire;
	}

	public void setWire(Wire w) {
		wire = w;
	}

	public Gate getParent() {
		return parent;
	}

	public void setParent(Gate gate) {
		parent = gate;
	}

	public boolean isSelected() {
		return selected;
	}

	@Override
	public Sprite getSprite() {
		return sprite;
	}

}
