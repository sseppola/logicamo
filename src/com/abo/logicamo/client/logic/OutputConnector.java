package com.abo.logicamo.client.logic;

import java.util.ArrayList;

import com.abo.logicamo.client.Globals;
import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.nodes.Button;
import com.abo.logicamo.client.graphics.nodes.Button.ButtonHandler;
import com.abo.logicamo.client.graphics.nodes.Sprite;

public class OutputConnector extends Connector {

	private Wire dragWire;
	private ArrayList<Wire> wires;

	public OutputConnector(boolean flatSide) {

		wires = new ArrayList<Wire>();

		isActive = false;
		selected = false;
		state = GateValue.UNDEFINED;

		if (flatSide) {
			if (active_image == null) {
				active_image = Loader.getImageById("connector_front_active");
			}
			if (normal_image == null) {
				normal_image = Loader.getImageById("connector_front_inactive");
			}
		} else {
			if (active_image == null) {
				active_image = Loader.getImageById("connector_active");
			}
			if (normal_image == null) {
				normal_image = Loader.getImageById("connector_inactive");
			}
		}
		sprite = new Sprite(normal_image);
		sprite.setPickingEnabled(true);

		remover = new Button(Loader.getImageById("button_remove"), new ButtonHandler() {
			@Override
			public void onRelease(Button b) {
			}

			@Override
			public void onPress(Button b) {
			}

			@Override
			public void onClick(Button b) {
				for (Wire w : wires) {
					Globals.mainloop.getWorkspace().queueWireForRemoval(w);
				}
				Logger.log(this, "wires added for removal");
			}
		});

		remover.setPosition(active_image.getWidth(), -remover.getWidth());
		remover.setOpacity(0.0);

		sprite.add(remover);
	}

	@Override
	public void update() {

		updatePictureAndActivity();
		updateMouse();

	}

	/**
	 * Create or remove dragwires depending on mouse position and clicks.
	 */
	private void updateMouse() {

		updateOrRemoveDragWire();

		if (!selected) {

			if (hasWires()
					&& ((!sprite.isMouseDown() && isActive) || remover
							.isMouseOver())) {
				remover.setOpacity(1.0);
			} else {
				remover.setOpacity(0.0);
			}

			if (isActive) {
				if (sprite.isMousePressed()) {
					startDragWire();
				} else if (sprite.isMouseReleased()) {
					if ((Globals.selectedConnector != null)
							&& (Globals.selectedConnector instanceof InputConnector)) {
						// We have a wire that has been started from an
						// inputconnector

						createAndSaveWire();
					} else {
						Logger.log(this, "connector might already be connected");
					}
				}
			}
		} else {
			if (sprite.isMouseReleased()) {
				selected = false;
			}
		}
	}

	/**
	 * A dragwire has been connected to this outputconnector from an
	 * inputconnector. A new wire is now created between the two connectors and
	 * saved in workspace's wirelist.
	 */
	private void createAndSaveWire() {

		// create a line between cons
		InputConnector inputcon = (InputConnector) Globals.selectedConnector;
		Wire w = new Wire(this, inputcon, parent, inputcon.parent);
		wires.add(w);
		inputcon.setWire(w);
		inputcon.setConnection(true);
		Globals.mainloop.getWorkspace().addToWires(w); // TODO: GET RID OF THIS
														// SPAGHETTI!!
		parent.connectTo(inputcon.getParent());
		Globals.mainloop.getWorkspace().addToScene(w);
	}

	/**
	 * A wire is created when mousepressing and dragging on the outputconnector
	 */
	private void startDragWire() {
		selected = true;
		dragWire = new Wire(this, null, parent, null);
		Globals.mainloop.getWorkspace().addToScene(dragWire);
	}

	/**
	 * An active dragwire is updated or removed
	 */
	private void updateOrRemoveDragWire() {
		if (dragWire != null) {
			dragWire.update();

			if (Globals.mainEngine.isMouseUp()) {
				dragWire.removeFromParent();
				dragWire = null;
			}
		}
	}

	/**
	 * The connector picture is switched between active and non-active depending
	 * on mouse position
	 */
	private void updatePictureAndActivity() {
		if (!isActive && sprite.isMouseOver()) {
			sprite.setImage(active_image);
			isActive = true;
		}
		if (isActive && !sprite.isMouseOver()) {
			if (!selected) {
				sprite.setImage(normal_image);
				isActive = false;
			}
		}
	}

	public boolean hasWires() {
		return wires.size() > 0;
	}

	public ArrayList<Wire> getWires() {
		return wires;
	}

	public boolean isSelected() {
		return selected;
	}

	public Gate getParent() {
		return parent;
	}

	public void setParent(Gate parent) {
		this.parent = parent;
	}

	@Override
	public Sprite getSprite() {
		return sprite;
	}

}
