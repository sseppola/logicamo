package com.abo.logicamo.client.logic;

import java.util.ArrayList;
import java.util.Arrays;

import com.abo.logicamo.client.Logger;

public abstract class StringModifier {

	/**
	 * Modify operators in string format AND/XOR/OR/NOT to operators that
	 * javascript understands, i.e. && ^ || !
	 * 
	 * The order of replaces is crucial since OR can match both with OR and XOR
	 * 
	 * @param oldtext
	 *            logic expression in string format, including operators in text
	 *            format
	 * @return logic expression with text operators replaced to javascript
	 *         format
	 */
	public static String modifyForJavascript(String oldtext) {

		oldtext = oldtext.replace("AND", "&&");
		oldtext = oldtext.replace("XOR", "^");
		oldtext = oldtext.replace("OR", "||");
		oldtext = oldtext.replace("NOT", "!");

		// now the values should be inside the string

		return oldtext;
	}

	/**
	 * Finding the logic expression without the name and the equals-sign
	 * 
	 * @param oldtext
	 *            the logic expression including its name and the equals-sign
	 * @return the logic expression without the name and the equals-sign
	 */
	public static String getBooleanExpression(String oldtext) {
		int eq_index = oldtext.indexOf('=');
		oldtext = oldtext.substring(eq_index + 1, oldtext.length());
		oldtext = oldtext.trim();

		return oldtext;
	}

	/**
	 * Finds the name from the logic expression, i.e. the name of the
	 * output-gate
	 * 
	 * @param oldtext
	 *            the logic expression in its original format
	 * @return name of outputgate Qn that generated the expression
	 */
	public static String getExpressionName(String oldtext) {

		int eq_index = oldtext.indexOf('=');
		oldtext = oldtext.substring(0, eq_index);
		oldtext = oldtext.replace(" ", "");

		return oldtext;
	}

	/**
	 * After removing all unnecessary operators, brackets and spaces, we are
	 * left with the variables in the logic expression
	 * 
	 * The order of replaces is crucial since OR can match with OR/XOR/NOR/XNOR,
	 * AND with NAND etc...
	 * 
	 * @param oldtext
	 *            the logic expression in its original format
	 * @return variables in a String array
	 */
	public static String[] getVariables(String oldtext) {

		oldtext = oldtext.replace("(", "  ");
		oldtext = oldtext.replace(")", "  ");
		oldtext = oldtext.replace("NAND", "  ");
		oldtext = oldtext.replace("AND", "  ");
		oldtext = oldtext.replace("&&", "  ");
		oldtext = oldtext.replace("XOR", "  ");
		oldtext = oldtext.replace("XNOR", "  ");
		oldtext = oldtext.replace("NOR", "  ");
		oldtext = oldtext.replace("OR", "  ");
		oldtext = oldtext.replace("||", "  ");
		oldtext = oldtext.replace("NOT", "  ");
		oldtext = oldtext.replace("!", "  ");
		oldtext = oldtext.replace("^", "  ");

		oldtext = oldtext.trim();

		ArrayList<String> var_list = new ArrayList<String>();
		String[] tokens = oldtext.split("\\s+");

		for (int i = 0, s = tokens.length; i < s; i++) {
			String var = tokens[i];
			if (!var_list.contains(var)) {
				var_list.add(var);
			}
		}

		String[] variables = new String[var_list.size()];
		variables = var_list.toArray(variables);

		Arrays.sort(variables);

		return variables;
	}

	/**
	 * Replace all the NAND/XNOR/NOR operators with normal AND/XOR/OR and NOT
	 * operators
	 * 
	 * @param oldtext
	 *            logic expression that might contain NAND/XNOR/NOR operators
	 * @return equivalent logic expression without the NAND/XNOR/NOR operators
	 */
	public static String modifySpecialOperators(String oldtext) {

		Logger.log(StringModifier.class, "original logic expression is = "
				+ oldtext);

		// first get hold of the expression, that's after the =
		int eq_index = oldtext.indexOf('=');
		oldtext = oldtext.substring(eq_index + 1);

		// now modify all the NAND, XNOR, and NOR -operators
		while (oldtext.contains("NAND") || oldtext.contains("XNOR")
				|| oldtext.contains("NOR")) {
			oldtext = modifyOneOperator(oldtext);
		}

		Logger.log(StringModifier.class, "modified logic expression is = "
				+ oldtext);
		return oldtext;
	}

	/**
	 * Replaces the NAND/XNOR/NOR operators with a NOT + brackets + AND/XOR/OR
	 * operator. Although the extra brackets are not needed every time, it
	 * doesn't hurt to add them either
	 * 
	 * @param oldtext
	 *            logical expression that includes a NAND, NOR or XNOR gate
	 * @return the same logic expression with one special operator replaced
	 */
	private static String modifyOneOperator(String oldtext) {

		int locationOfOperator;
		int locationToPutNot = 0;
		int locationToPutClosingBracket = oldtext.length();

		String operator;

		if (oldtext.contains("NAND")) {
			operator = "NAND";

		} else if (oldtext.contains("XNOR")) {
			operator = "XNOR";

		} else {
			operator = "NOR";
		}

		locationOfOperator = oldtext.lastIndexOf(operator);
		locationToPutNot = findLeftBracketPlace(oldtext, locationOfOperator);
		locationToPutClosingBracket = findRightBracketPlace(oldtext,
				locationOfOperator + operator.length());

		oldtext = createStringWithNotAndBrackets(oldtext, locationOfOperator,
				locationToPutNot, locationToPutClosingBracket, operator);

		Logger.log(StringModifier.class, "removed one " + operator);

		return oldtext;
	}

	/**
	 * String operations for replacing the old logic expression with the new one
	 * 
	 * @param oldtext
	 *            logic expression in its old format
	 * @param locationOfOperator
	 *            index of the special operator NOR/NAND/XNOR that needs to be
	 *            replaced
	 * @param locationToPutNot
	 *            index of where the NOT and the left bracket will be added to
	 * @param locationToPutClosingBracket
	 *            index of where the right bracket will be added to
	 * @param operator
	 *            the special NOR/NAND/XNOR operator
	 * @return logic expression in its new format
	 */
	private static String createStringWithNotAndBrackets(String oldtext,
			int locationOfOperator, int locationToPutNot,
			int locationToPutClosingBracket, String operator) {

		String newtext;

		// insert closing bracket
		if (locationToPutClosingBracket == oldtext.length()) {
			newtext = oldtext + "))";
		} else {
			newtext = oldtext.substring(0, locationToPutClosingBracket)
					+ "))"
					+ oldtext.substring(locationToPutClosingBracket,
							oldtext.length());
		}

		// replace operator with the un-negated operator
		if (operator.equals("NAND")) {
			newtext = newtext.substring(0, locationOfOperator)
					+ "AND"
					+ newtext.substring(locationOfOperator + 4,
							newtext.length());
		} else if (operator.equals("XNOR")) {
			newtext = newtext.substring(0, locationOfOperator)
					+ "XOR"
					+ newtext.substring(locationOfOperator + 4,
							newtext.length());
		} else {
			// operator equals "NOR"
			newtext = newtext.substring(0, locationOfOperator)
					+ "OR"
					+ newtext.substring(locationOfOperator + 3,
							newtext.length());
		}

		// insert "NOT" and opening bracket
		newtext = newtext.substring(0, locationToPutNot) + "(NOT("
				+ newtext.substring(locationToPutNot, newtext.length());

		return newtext;
	}

	/**
	 * finds the place where to put the NOT and the left bracket
	 * 
	 * @param oldtext
	 *            the logic expression in its old format
	 * @param locationOfOperator
	 *            index of special NOR/NAND/XNOR operator
	 * @return index of where to put the NOT and the left bracket
	 */
	private static int findLeftBracketPlace(String oldtext,
			int locationOfOperator) {

		int openBrackets = 0;
		int locationToPutNot = 0;

		for (int i = locationOfOperator - 1; i >= 0; i--) {

			char curr = oldtext.charAt(i);
		
			if(openBrackets > 0){
				if(curr == '('){
					openBrackets--;
				}else if(curr == ')'){
					openBrackets++;
				}
			} else{
				if(curr == '('){
					locationToPutNot = i;
					break;
				} else if(curr == ')'){
					openBrackets++;
				}
			}
		}

//			if (!operandFound) {
//				if (curr == ' ') {
//					// skip
//				} else if (curr == ')') {
//					operandFound = true;
//				} else {
//					operandFound = true;
//					locationToPutNot = i;
//				}
//			} else {
//				if (locationToPutNot > 0) {
//					// we have found the operator but it might be longer than 1
//					// char TODO: check if has NOT or many NOT
//					if (curr != ' ' && curr != '(') {
//						locationToPutNot = i;
//					} else if (curr == ' ' && i >= 3) {
//						if (oldtext.substring(i - 3, i).equals("NOT")) {
//							locationToPutNot = i - 3;
//							break;
//						}
//					} else {
//						break;
//					}
//				} else if (openBrackets > 0) {
//					// we have are inside the operator that is inside brackets
//					if (curr == ')') {
//						openBrackets++;
//					} else if (curr == '(') {
//						openBrackets--;
//						if (openBrackets == 0) {
//							locationToPutNot = i;
//							break;
//						}
//					}
//
//				}
//			}
//		}

		// // find location for NOT and the left bracket
		// for (int i = locationOfOperator; i >= 0; i--) {
		//
		// char curr = oldtext.charAt(i);
		//
		// if ((!operandFound || openBrackets > 0) && curr == ')') {
		// openBrackets++;
		// } else if (curr == ')') {
		// // LOCATION FOUND! we will put the negation after the
		// // bracket
		// locationToPutNot = i;
		// break;
		// } else if (openBrackets > 0 && curr == '(') {
		// openBrackets--;
		// } else if (curr == '(') {
		// // LOCATION FOUND! we will put the negation before the
		// // bracket
		// locationToPutNot = i;
		// break;
		// } else if (curr == ' ') {
		// // do nothing, skip empty chars
		// } else {
		// operandFound = true;
		// }
		// }

		return locationToPutNot;
	}

	/**
	 * finds the place where to put the right bracket
	 * 
	 * @param oldtext
	 *            the logic expression in its old format
	 * @param locationOfOperator
	 *            index of special NOR/NAND/XNOR operator
	 * @return index of where to put the right bracket
	 */
	private static int findRightBracketPlace(String oldtext,
			int locationAfterOperator) {

		int openBrackets = 0;
		int locationToPutClosingBracket = oldtext.length();

		for (int i = locationAfterOperator; i < oldtext.length(); i++) {

			char curr = oldtext.charAt(i);

			if(openBrackets > 0){
				if(curr == ')'){
					openBrackets--;
				} else if(curr == '('){
					openBrackets++;
				}
			}else{
				if(curr == ')'){
					locationToPutClosingBracket = i+1;
					break;
				} else if(curr == '('){
					openBrackets++;
				}
			}
		}
			
//			if (!operandFound) {
//				if (curr == ' ') {
//					// skip
//				} else if (curr == '(') {
//					operandFound = true;
//				} else {
//					operandFound = true;
//					locationToPutClosingBracket = i + 1;
//				}
//			} else {
//				if (locationToPutClosingBracket < oldtext.length()) {
//					if (curr != ' ' && curr != ')') {
//						locationToPutClosingBracket = i + 1;
//					} else {
//						break;
//					}
//				} else if (openBrackets > 0) {
//					if (curr == '(') {
//						openBrackets++;
//					} else if (curr == ')') {
//						openBrackets--;
//						if (openBrackets == 0) {
//							locationToPutClosingBracket = i + 1;
//							break;
//						}
//					}
//				}
//			}

		// // find location for the right bracket
		// for (int i = locationAfterOperator; i < oldtext.length(); i++) {
		//
		// char curr = oldtext.charAt(i);
		//
		// if ((!operandFound || openBrackets > 0) && curr == '(') {
		// openBrackets++;
		// } else if (curr == '(') {
		// // LOCATION FOUND! we will put the bracket before the old one
		// locationToPutClosingBracket = i;
		// break;
		// } else if (openBrackets > 0 && curr == ')') {
		// openBrackets--;
		// } else if (curr == ')') {
		// // LOCATION FOUND! we will put the bracket after the old one
		// locationToPutClosingBracket = i + 1;
		// break;
		// } else if (curr == ' ') {
		// // do nothing, skip empty chars
		// } else {
		// operandFound = true;
		// }
		// }

		return locationToPutClosingBracket;
	}
}
