package com.abo.logicamo.client.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.abo.logicamo.client.Globals;
import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.graphics.loader.Loader;
import com.abo.logicamo.client.graphics.nodes.Button;
import com.abo.logicamo.client.graphics.nodes.Button.ButtonHandler;
import com.abo.logicamo.client.graphics.nodes.Sprite;
import com.google.gwt.user.client.ui.Image;

/**
 * Abstract class that provides a common interface to all logic gate types.
 * 
 * @author Sara
 * @version 1.0
 */
public abstract class Gate extends Sprite {

	private final Button removeButton;
	private final Set<Gate> connections;
	
	private final List<InputConnector> inputConnectors;
	private final OutputConnector outputConnector;
	private double inputOffsetX;
	private boolean canDrag;

	private final String name;
	private final int gateId;
	private static int gateIDCounter = 1;

	private final int[] inputStates;
	private int outputState;

	protected Gate(String gate_image_id, String name, int numInputs) {

		// Set up sprite
		if (gate_image_id != null) {
			Image image = Loader.getImageById(gate_image_id);
			setImage(image);
		}
		
		connections = new HashSet<>();

		// Create remover
		removeButton = new Button(Loader.getImageById("button_remove"), new ButtonHandler() {
			@Override
			public void onRelease(Button b) {
			}

			@Override
			public void onPress(Button b) {
			}

			@Override
			public void onClick(Button b) {
				Globals.mainloop.getWorkspace().queueGateForRemoval(Gate.this);
				Logger.log(this, "gate added for removal");
			}
		});

		double half = removeButton.getWidth() / 2;
		removeButton.setPosition(-half, -half);
		removeButton.setOpacity(0.0);
		add(removeButton);

		this.name = name;

		// Set up gate ID and state
		gateId = gateIDCounter;
		gateIDCounter++;
		outputState = GateValue.UNDEFINED;

		// Set up inputs
		inputOffsetX = 0;
		inputStates = new int[numInputs];
		inputConnectors = new ArrayList<InputConnector>();
		for (int i = 0; i < numInputs; ++i) {
			add(new InputConnector(false));
		}

		// Set up output connector (one-time operation)
		outputConnector = new OutputConnector(false);
		outputConnector.getSprite().setPosition(getWidth() - 1,
				(getHeight() / 2) - (0.5 * getOutput().getSprite().getHeight()));
		outputConnector.setParent(this);
		add(outputConnector.getSprite());

		canDrag = true;

		// Log successful creation of gate
		Logger.log(this, "gate number " + gateId + " created");
	}

	protected void add(InputConnector ic) {
		inputConnectors.add(ic);
		ic.setParent(this);
		add(ic.getSprite());
		repositionInputs();
	}

	private void repositionInputs() {
		int count = inputConnectors.size();

		if (count == 1) {
			// Just place the damn thing in the damn middle
			Sprite s = getInput(0).getSprite();
			s.setPosition(-(s.getWidth() - inputOffsetX), getHeight() / 2.0 - (s.getHeight() / 2.0));
		} else {
			// Use this broken algorithm to do an even spacing...
			double range = (getHeight() / 3.0) * 2.0;
			double step_y = range / count;
			double half_y = getHeight() / 2.0;
			double start_y = half_y - (range / 4.0);

			for (int i = 0; i < count; ++i) {
				Sprite s = getInput(i).getSprite();
				s.setPosition(-(s.getWidth() - inputOffsetX), start_y + (step_y * i) - (s.getHeight() / 2.0));
			}
		}
	}

	protected void setInputXOffset(double offset) {
		inputOffsetX = offset;
		repositionInputs();
	}

	protected OutputConnector getOutput() {
		return outputConnector;
	}

	public InputConnector getInput(int idx) {
		return inputConnectors.get(idx);
	}

	public int getInputCount() {
		return inputConnectors.size();
	}

	public String getName() {
		return name;
	}

	public void setState(int state) {
		outputState = state;
		outputConnector.setState(state);
	}

	public int getState() {
		return outputState;
	}

	protected void disableDragging() {
		canDrag = false;
	}

	/**
	 * Checks if a connector is selected and calls update() on all connectors
	 * and sets Globals.selectedConnector if necessary Also checks if a gate is
	 * selected
	 * 
	 * @return true if a connector is selected
	 */
	public boolean updateMouse() {

		if (canDrag) {
			if (dragging && !isMouseDown()) {
				stopDrag();
			}
			if (isMousePressed() && isMouseOver()) {
				startDrag();
				bringToFront();
			}
		}

		Connector selected = null;

		int inputs = inputConnectors.size();
		InputConnector inputcon;

		// updates all input connectors and checks if an input connector is
		// selected
		for (int i = 0; i < inputs; i++) {
			inputcon = inputConnectors.get(i);
			inputcon.update();
			if (inputcon.isSelected() && !inputcon.hasConnection()) {
				selected = inputcon;
			}
		}

		// updates the output connector and checks if it is selected
		outputConnector.update();
		if (outputConnector.isSelected()) {
			selected = outputConnector;
		}

		if ((isMouseOver() && !isMouseDown()) || removeButton.isMouseOver()) {
			removeButton.setOpacity(1.0);
		} else {
			removeButton.setOpacity(0.0);
		}

		if (selected != null) {
			// there is a selectionState
			Globals.selectedConnector = selected;
			return true;
		}

		// no selection in this gate
		return false;
	}

	/**
	 * Counts the gates' logic state and updates output connector value
	 */
	public void updateLogic() {

		final int inputCount = inputConnectors.size();
		int values = 0;

		boolean[] inputValues = new boolean[inputCount];
		InputConnector input;

		// counts the number of correct input values and calculates the output
		// value if needed
		for (int i = 0; i < inputCount; i++) {

			input = inputConnectors.get(i);
			int inState = input.getState();
			inputStates[i] = inState;

			if (inState != GateValue.UNDEFINED) {
				values++;
			}
			inputValues[i] = (inState == GateValue.TRUE);

		}

		// Process gate logic
		if (values == inputCount) {
			process(inputValues);
		} else {
			outputConnector.setState(GateValue.UNDEFINED);
			processUndefined(inputStates);
		}
	}

	public int getGateId() {
		return gateId;
	}

	/**
	 * Change the state of an input.
	 * 
	 * @param index
	 *            Index of the input you want to change.
	 * @param value
	 *            The new value of the input.
	 */
	public void setInput(int index, int value) {
		assert(index >= 0 && index < inputStates.length) : "Input state index out of bounds";
		inputStates[index] = value;
	}

	/**
	 * Processes the inputs according to the gate's logic.
	 * 
	 * @param inputs
	 *            Array containing all the input values.
	 * @return The output value processed by this gate.
	 */
	public abstract boolean process(boolean[] inputs);

	/**
	 * Tries to process inputs with insufficient information
	 * 
	 * @param inputs
	 */
	public void processUndefined(int[] inputs) {
	}
	
	public void connectTo(Gate gate) {
		connections.add(gate);
	}

	public boolean isConnectedTo(Gate gate) {
		return connections.contains(gate);
	}

	public void removeConnection(Gate gate) {
		if (isConnectedTo(gate)) {
			connections.remove(gate);
		}
	}

	public abstract <T extends Gate> T makeClone();

}
