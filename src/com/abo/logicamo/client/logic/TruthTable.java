package com.abo.logicamo.client.logic;

import java.util.ArrayList;

import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.logic.LogicTree.TreeNode;

/**
 * A class to contain information about a truth table corresponding to the
 * circuit
 * 
 * @author sseppola
 *
 */
public class TruthTable {

	private int input_amount;
	private int lines;

	private int output_amount;
	private int[][] truthtable;
	private String[][] truthtable_output;
	private ArrayList<String> inputNames;
	private ArrayList<TreeNode> treeroots;

	private String exp_name;
	private String exp_jscript;
	private String exp;
	private String expression_as_native; // we need to get rid of all NAND, NOR and XNOR
									// operators
	private String[] vars;

	public TruthTable(String expression) {

		exp_name = StringModifier.getExpressionName(expression);
		exp = StringModifier.getBooleanExpression(expression);
		expression_as_native = StringModifier.modifySpecialOperators(expression);

		// exp_jscript contains the expression in a logical form that JavaScript
		// can understand
		exp_jscript = StringModifier.modifyForJavascript(expression_as_native);

		// vars contains all variables in this expression
		vars = StringModifier.getVariables(exp_jscript);

		input_amount = vars.length;
		lines = (int) Math.pow(2, input_amount);

		truthtable = new int[lines][input_amount + 1];
		truthtable_output = new String[lines + 1][input_amount + 1];

		// initialize stuff
		for (int i = 0; i < lines; i++) {
			for (int j = 0; j < input_amount; j++) {
				if (((i / (lines / (int) Math.pow(2, j + 1))) % 2) == 0) {
					truthtable[i][j] = GateValue.FALSE;
					truthtable_output[i + 1][j] = "0";
				} else {
					truthtable[i][j] = GateValue.TRUE;
					truthtable_output[i + 1][j] = "1";
				}
			}
		}

		for (int i = 0; i < input_amount; i++) {
			truthtable_output[0][i] = vars[i];
		}
		truthtable_output[0][input_amount] = exp;

		// fix values inside the tt with jscript
		for (int i = 0; i < lines; i++) {

			// make a copy of the javascript-expression
			String exp_temp = exp_jscript;

			// exchange variables in expression for values per line
			for (int j = 0; j < input_amount; j++) {
				String var_name = vars[j];
				String var_val = truthtable_output[i + 1][j];
				exp_temp = exp_temp.replace(var_name, var_val);
			}

			// now we should have a nice expression of type "1 && (0 ||�1)" that
			// can be calculated with jscript
			boolean line_val = evaluateBooleanExpression(exp_temp);

			String line_val_str = "0";
			int line_val_int = GateValue.FALSE;

			if (line_val == true) {
				line_val_str = "1";
				line_val_int = GateValue.TRUE;
			}

			truthtable[i][input_amount] = line_val_int;
			truthtable_output[i + 1][input_amount] = line_val_str;
		}

		// output
		for (int i = 0; i < (lines + 1); i++) {
			String line = "";
			for (int j = 0; j < (input_amount + 1); j++) {
				line = line + truthtable_output[i][j] + " ";
			}
			Logger.log(this, line);
		}
	}

	// javascript is nice and changes my value types...
	public static native boolean evaluateBooleanExpression(String expr) /*-{
		var result = false;
		void (result = eval(expr));
		return result != 0;
	}-*/;

	public String getName() {
		return exp_name;
	}

	public String getExpression() {
		return exp_jscript;
	}

	public ArrayList<String> getInputNames() {
		return inputNames;
	}

	public String[][] getTTOutput() {
		return truthtable_output;
	}

	public TruthTable(LogicTree tree) {

		treeroots = tree.getRoots();

		inputNames = tree.getInputNames();
		output_amount = treeroots.size();
		input_amount = inputNames.size();

		treeroots = tree.getRoots();
		int lines = (int) Math.pow(2, input_amount);
		truthtable = new int[lines][input_amount + output_amount];
		truthtable_output = new String[lines + 1][input_amount + output_amount];

		// initialize stuff
		for (int i = 0; i < lines; i++) {
			for (int j = 0; j < input_amount; j++) {
				if (((i / (lines / (int) Math.pow(2, j + 1))) % 2) == 0) {
					truthtable[i][j] = GateValue.FALSE;
					truthtable_output[i + 1][j] = "0";
				} else {
					truthtable[i][j] = GateValue.TRUE;
					truthtable_output[i + 1][j] = "1";
				}
			}
		}

		for (int i = 0; i < input_amount; i++) {
			truthtable_output[0][i] = inputNames.get(i);
		}
		for (int i = input_amount; i < (input_amount + output_amount); i++) {
			truthtable_output[0][i] = treeroots.get(i - input_amount).getName();
		}

		Logger.log(this, "lines = " + lines);
		for (int i = 0; i < (lines + 1); i++) {
			String line = "";
			for (int j = 0; j < (input_amount + output_amount); j++) {
				line = line + truthtable_output[i][j] + " ";
			}
		}
	}

	public int getRows() {
		return lines + 1;
	}

	public int getColumns() {
		return input_amount + 1;
	}
}
