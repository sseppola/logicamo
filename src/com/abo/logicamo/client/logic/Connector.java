package com.abo.logicamo.client.logic;

import com.abo.logicamo.client.graphics.nodes.Button;
import com.abo.logicamo.client.graphics.nodes.Sprite;
import com.google.gwt.user.client.ui.Image;

public abstract class Connector{

	protected Gate parent;
	protected Sprite sprite;
	protected Image active_image;
	protected Image normal_image;
	protected boolean isActive;			// mouse is over
	protected boolean selected;			// mouse is pressed
	protected int state;
	protected Button remover;

	public void setState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}

	public abstract Sprite getSprite();

	public abstract void update();
	
}
