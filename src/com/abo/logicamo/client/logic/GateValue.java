package com.abo.logicamo.client.logic;

public class GateValue {

	public static final int TRUE = 2;
	public static final int FALSE = 1;
	public static final int UNDEFINED = 0;

	public static final String AND = "AND";
	public static final String OR = "OR";
	public static final String NOT = "NOT";
	public static final String NAND = "NAND";
	public static final String NOR = "NOR";
	public static final String XOR = "XOR";
	public static final String XNOR = "XNOR";

}
