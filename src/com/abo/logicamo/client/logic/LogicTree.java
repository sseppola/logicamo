package com.abo.logicamo.client.logic;

import java.util.ArrayList;

import com.abo.logicamo.client.Globals;
import com.abo.logicamo.client.Logger;
import com.abo.logicamo.client.logic.gates.BillGate;
import com.abo.logicamo.client.logic.gates.InputGate;
import com.abo.logicamo.client.logic.gates.OutputGate;

/**
 * A class to represent the logic tree for a circuit. Tree roots are OutputGates
 * 
 * @author sseppola
 *
 */
public class LogicTree {

	public static class TreeNode {
		private ArrayList<TreeNode> children;
		private String name;
		private Gate gate;

		public TreeNode(Gate gate) {
			this.gate = gate;
			name = gate.getName();
			children = new ArrayList<TreeNode>();
		}

		public Gate getGate() {
			return gate;
		}

		public String getName() {
			return name;
		}

		public void addChild(TreeNode n) {
			children.add(n);
		}
	}
	
	private ArrayList<TreeNode> roots;
	private ArrayList<String> inputgate_names;
	private ArrayList<Integer> parentgateid;

	private String[] expressions;
	private int amount_of_roots;

	/**
	 * Create tree with one specified root. This is called from a specific
	 * OutputGate.
	 */
	public LogicTree(OutputGate o) {
		roots = new ArrayList<TreeNode>();
		parentgateid = new ArrayList<Integer>();
		inputgate_names = new ArrayList<String>();

		roots.add(new TreeNode(o));

		expressions = new String[1];

		TreeNode root = roots.get(0);
		String gatename = root.getName();
		expressions[0] = gatename + " = " + traverse(roots.get(0));
	}

	/**
	 * Create roots and a logic tree for the whole circuit (or all of them on
	 * workspace)
	 * 
	 * This is currently not used but code is not removed if somebody later on
	 * finds a need for this :-) July2014
	 * 
	 * @author Sara Seppola
	 * 
	 */
	public LogicTree() {

		roots = new ArrayList<TreeNode>();
		parentgateid = new ArrayList<Integer>();
		inputgate_names = new ArrayList<String>();

		ArrayList<OutputGate> outputgates = Globals.mainloop.getWorkspace()
				.getOutputGates();
		for (OutputGate o : outputgates) {
			if (o.getInput(0).hasConnection()) {
				Logger.log(this, "a root is created");
				roots.add(new TreeNode(o));
			}
		}

		amount_of_roots = roots.size();
		expressions = new String[amount_of_roots];
		if (amount_of_roots > 0) {
			expressions = new String[amount_of_roots];

			for (int i = 0; i < amount_of_roots; i++) {
				TreeNode root = roots.get(i);
				String gatename = root.getName();
				expressions[i] = gatename + " = " + traverse(roots.get(i));
			}
		}
	}

	String traverse(TreeNode node) {

		String nodevalue = "";

		if (node.getGate() instanceof InputGate) {
			if (!inputgate_names.contains(node.getGate().getName())) {
				inputgate_names.add(node.getGate().getName());
			}
			return node.getName();
		} else if (node.getGate() instanceof BillGate) {
			return node.getName();
		} else {
			String childval = "";

			int enough = node.getGate().getInputCount();
			for (int i = 0, s = enough; i < s; i++) {
				TreeNode child = null;

				InputConnector input = node.getGate().getInput(i);
				if (input.hasConnection()) {
					enough--;

					Gate fromgate = input.getWire().getFromGate();

					if (parentgateid.contains(fromgate.getGateId())) {
						// fix a dumb node in case of loops inside circuit
						fromgate = new BillGate();
					}
					parentgateid.add(new Integer(fromgate.getGateId()));

					child = new TreeNode(fromgate);

					node.addChild(child);
					childval = traverse(child);

					if (!(node.getGate() instanceof OutputGate)
							&& (child.getGate().getInputCount() > 1)) {
						childval = "(" + childval + ")";
					}

					parentgateid.remove((Integer) fromgate.getGateId());

					// dirty hack
					if (childval.equals("")) {
						enough++;
					}
				} else {
					// an input is not connected, this should return error
					return "ERROR";
				}

				if (s > 1) {
					if (i == 0) {
						nodevalue = childval;
					} else {
						nodevalue = nodevalue + " " + node.getGate().getName() + " "
								+ childval;
					}
				} else {
					if (node.getGate() instanceof OutputGate) {
						nodevalue = childval;
						return nodevalue;
					} else {
						nodevalue = node.getGate().getName() + " " + childval;
					}
				}
			}
			if (enough > 0) {
				nodevalue = "";
			}
		}
		return nodevalue;
		// return "error in traverse";
	}

	public String[] getExpressions() {
		return expressions;
	}

	public ArrayList<String> getInputNames() {
		return inputgate_names;
	}

	public ArrayList<TreeNode> getRoots() {
		return roots;
	}

}
