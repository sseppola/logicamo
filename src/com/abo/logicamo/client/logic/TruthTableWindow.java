package com.abo.logicamo.client.logic;

import com.abo.logicamo.client.graphics.DrawUtil;
import com.abo.logicamo.client.graphics.Node;
import com.abo.logicamo.client.graphics.Scene;
import com.abo.logicamo.client.graphics.nodes.Text;
import com.abo.logicamo.client.graphics.nodes.Window;
import com.abo.logicamo.client.logic.gates.OutputGate;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.Context2d.TextBaseline;

public class TruthTableWindow extends Window {

	private OutputGate output;
	private String expressionString;
	private Canvas tableCanvas;

	public TruthTableWindow(String expression, OutputGate output) {
		super("Truth table for " + output.getName());
		tableCanvas = Canvas.createIfSupported();
		expressionString = expression;
		this.output = output;
		drawTable();
	}

	public OutputGate getOutput() {
		return output;
	}

	public String getExpression() {
		return expressionString;
	}

	public void makePassive() {
		setOpacity(0.7);
	}

	private void drawTable() {

		Scene scn = new Scene(tableCanvas);
		TruthTable tt = new TruthTable(expressionString);
		String[][] ttdata = tt.getTTOutput();
		Node table = new Node();

		final int lineWidth = 3;
		final int spacing = 1;
		final int textSize = 12;

		final int cols = tt.getColumns();
		final int rows = tt.getRows();

		// Add table text
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < cols; ++j) {
				Text temp = new Text(ttdata[i][j]);
				temp.setFont(textSize, "Arial");
				temp.setTextBaseline(TextBaseline.BOTTOM);
				temp.setPosition(lineWidth + spacing + (j * (textSize + (2 * spacing) + lineWidth)),
						lineWidth + ((i + 1) * textSize) + (i * (lineWidth + (2 * spacing))));
				table.add(temp);
			}

		}

		// Calculate required content size
		Text temp = new Text(ttdata[0][cols - 1]);
		temp.setFont(textSize, "Arial");
		int width = (int) (((cols - 1) * (textSize + (2 * spacing) + lineWidth)) + temp.getWidth() + (2 * spacing)
				+ (2 * lineWidth));
		int height = ((rows * (textSize + (2 * spacing) + lineWidth))) - lineWidth;

		// Set Window object size
		setSize(width, height);

		// Adjust table canvas size
		width = getContentAreaWidth();
		height = getContentAreaHeight();
		DrawUtil.setCanvasSize(tableCanvas, width, height);

		// Draw table canvas background
		Context2d ctx = tableCanvas.getContext2d();
		ctx.clearRect(0, 0, width, height);
		DrawUtil.drawRoundRect(ctx, 0, 0, width, height, 5, "white", null);

		// Draw gridlines for table
		ctx.setLineWidth(lineWidth);
		ctx.setStrokeStyle("black");
		ctx.setLineWidth(1);
		ctx.beginPath();
		
		for (int i = 1; i < cols; i++) {
			ctx.moveTo(i * (textSize + (2 * spacing) + lineWidth), 0);
			ctx.lineTo(i * (textSize + (2 * spacing) + lineWidth), height);
		}

		for (int i = 1; i < rows; i++) {
			ctx.moveTo(0, (i * (textSize + (2 * spacing) + lineWidth)) - 1);
			ctx.lineTo(width, (i * (textSize + (2 * spacing) + lineWidth)) - 1);
		}
		ctx.stroke();
		ctx.beginPath();

		// Set node size (not really necessary but good form)
		table.setSize(width, height);

		// Add table to scene and draw to create table canvas content
		scn.add(table);
		scn.setAutoClear(false);
		scn.draw();
		setContent(tableCanvas);

		// Clear links to enable GC to work
		table.clear();
		scn.clearChildren();

	}

}
