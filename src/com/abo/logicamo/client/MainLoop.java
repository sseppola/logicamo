package com.abo.logicamo.client;

import com.abo.logicamo.client.graphics.Engine;
import com.abo.logicamo.client.workspace.Gatebar;
import com.abo.logicamo.client.workspace.Workspace;

/**
 * Runs the program logic. The gatebar and the workspace are created here. The
 * update function calls updates also for the gatebar and the workspace.
 * 
 * @author Sara
 */
public class MainLoop {
	
	private Workspace workspace;
	private Gatebar gatebar;

	MainLoop(Engine engine) {
		Globals.mainloop = this;
		Logger.log(this, "mainloop is created");

		gatebar = new Gatebar(engine);
		Logger.log(this, "gatebar was created");

		workspace = new Workspace(engine);
		Logger.log(this, "workspace was created");

		gatebar.setWorkspace(workspace);
		Logger.log(this, "end of mainloop constructor");
	}

	/**
	 * The update loop for the program. In each phase, both the gatebar and the
	 * workspace are updated.
	 */
	public void update() {
		workspace.update();
	}

	public Workspace getWorkspace() {
		return workspace;
	}

	public Gatebar getGatebar() {
		return gatebar;
	}
}
