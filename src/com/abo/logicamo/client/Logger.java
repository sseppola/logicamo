package com.abo.logicamo.client;

/**
 * Very simple logging wrapper, helps differentiate source of log messages
 * 
 * Written by Patrik Lindström
 * 
 * No rights reserved, may be freely used and distributed without limitations.
 */
public abstract class Logger {

	public static <T> void log(Class<T> sender, String str) {
		output_log("[INFO] " + sender.getSimpleName() + ": " + str);
	}
	
	public static <T> void log(T sender, String str) {
		output_log("[INFO] " + sender.getClass().getSimpleName() + ": " + str);
	}
	
	public static <T> void warn(Class<T> sender, String str) {
		output_warn("[WARN] " + sender.getSimpleName() + ": " + str);
	}
	
	public static <T> void warn(T sender, String str) {
		output_warn("[WARN] " + sender.getClass().getSimpleName() + ": " + str);
	}
	
	public static <T> void error(Class<T> sender, String str) {
		output_err("[ERROR] " + sender.getSimpleName() + ": " + str);
	}
	
	public static <T> void error(T sender, String str) {
		output_err("[ERROR] " + sender.getClass().getSimpleName() + ": " + str);
	}
	
	private static native void output_log(String str) /*-{
		try {
			console.log(str);
		} catch(ignore) {}
	}-*/;
	
	private static native void output_warn(String str) /*-{
		try {
			console.warn(str);
		} catch(ignore) {
			try {
				console.log(str);
			} catch(ignore2) {}
		}
	}-*/;
	
	private static native void output_err(String str) /*-{
		try {
			console.error(str);
		} catch(ignore) {
			try {
				console.log(str);
			} catch(ignore2) {}
		}
	}-*/;

}
