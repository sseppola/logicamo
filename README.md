# Logicamo

_Logicamo_ is an educational tool for teaching logic circuits, developed together with the teaching staff of �bo Akademi University. _Logicamo_ is a web based tool intended for newcomers in the field. Its main focus is on usability and clearing up the connection between mathematics and computer science.

With _Logicamo_ you are able to build logic circuits, define their input signals and get a correct output signal. You can also generate a logic expression for to the circuit as well as its truth table.

At the moment you can test _Logicamo_ in the address <http://users.abo.fi/sseppola/Logicamo>

### Use of Logicamo

You can _drag and drop_ gates to your circuit in the middle. When you want to connect two gates, you drag and drop a signal from one gates input connector to another gates output connector or vice versa.

**Gates** are located on top of the screen on the gate bar area

**Input gates** are located on the left of the screen on the input gate area

**Output gates** are located on the right of the screen on the output gate area

The **scene** is the area in the middle where the circuit will be created

Both the input gates and the output gates pull double duty as _buttons_. When you want a defined signal from an input gate, you _click_ on it and you can choose between 1 and 0. If you have a working circuit with defined input signals, its output signals will be displayed in the output gates. If you want to display a logic expression or a truth table for the circuit, you _click_ on its output gate.



\- Sara Seppola \-
